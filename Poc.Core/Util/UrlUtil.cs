using System.Web;

namespace Poc.Core.Util
{
	/// <summary>
	/// The UrlUtil class contains methods for url creation and manipulation.
	/// </summary>
	public class UrlUtil
	{
		private UrlUtil()
		{
		}

		/// <summary>
		/// GetApplicationPath returns the base application path and ensures that it allways ends with a "/".
		/// </summary>
		/// <returns></returns>
		public static string GetApplicationPath()
		{
			return Text.EnsureTrailingSlash(HttpContext.Current.Request.ApplicationPath);
		}

		/// <summary>
		/// Get the (lowercase) url of the site without any trailing slashes.
		/// </summary>
		/// <returns></returns>
		public static string GetSiteUrl()
		{
			string path = HttpContext.Current.Request.ApplicationPath;
			if (path.EndsWith("/") && path.Length == 1)
			{
				return GetHostUrl();
			}
			else
			{
				return GetHostUrl() + path.ToLower();
			}
		}

		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="pathInfo"></param>
		/// <returns></returns>
		public static string[] GetParamsFromPathInfo(string pathInfo)
		{
			if (pathInfo.Length > 0)
			{
				if (pathInfo.EndsWith("/"))
				{
					pathInfo = pathInfo.Substring(0, pathInfo.Length - 1);
				}
				pathInfo = pathInfo.Substring(1, pathInfo.Length -1);
				return pathInfo.Split(new char[] {'/'});
			}
			else
			{
				return null;
			}
		}
        
		private static string GetHostUrl()
		{
			string securePort = HttpContext.Current.Request.ServerVariables["SERVER_PORT_SECURE"];
			string protocol = securePort == null || securePort == "0" ? "http" : "https";
			string serverPort = HttpContext.Current.Request.ServerVariables["SERVER_PORT"];
			string port = serverPort == "80" ? string.Empty : ":" + serverPort;
			string serverName = HttpContext.Current.Request.ServerVariables["SERVER_NAME"];
			return string.Format("{0}://{1}{2}" , protocol, serverName, port ); 
		}
	}
}