using System;
using System.Configuration;
using System.Xml;
using System.Collections.Specialized;

namespace Poc.Core.Util
{
	/// <summary>
	/// Summary description for Config.
	/// </summary>
	public class Config
	{
		private Config()
		{
		}
        
		public static NameValueCollection GetConfiguration()
		{
		    return (NameValueCollection)ConfigurationManager.GetSection("PocSettings");
		}
	}

	public class PocSectionHandler : NameValueSectionHandler
	{
		protected override string KeyAttributeName
		{
			get { return "setting";	}
		}

		protected override string ValueAttributeName
		{
			get { return base.ValueAttributeName; }
		}
	}
}
