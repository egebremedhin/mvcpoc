﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Poc.Core.Entities.AdminModule;

namespace Poc.Core
{
    public interface IPocContext
    {
        /// <summary>
        /// The Poc user for the current request.
        /// </summary>
        AppUser CurrentUser { get; }

        /// <summary>
        /// Set the Poc user for the current context.
        /// </summary>
        /// <param name="user">The current user</param>
        void SetUser(AppUser user);

       
    }
}
