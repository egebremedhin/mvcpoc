﻿namespace Poc.Core.Enumerations
{
    public enum AreaAccessRight
    {
        Global              = 0,
        AreaAccessLevel1    = 1,
        AreaAccessLevel2    = 2,
        AreaAccessLevel3    = 3,
        FacilityAccessLevel = 4
    }
}
