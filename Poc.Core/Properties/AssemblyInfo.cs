﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security;

[assembly: CLSCompliant(false)]
[assembly: ComVisibleAttribute(false)]
[assembly: AssemblyTitleAttribute("Poc.Core for Microsoft .NET Framework 3.5")]
[assembly: AssemblyDescriptionAttribute("")]
[assembly: AssemblyCompanyAttribute("Poc Project")]
[assembly: AssemblyProductAttribute("Poc")]
[assembly: AssemblyCopyrightAttribute("2004-2007 Cuyahoga Project. All rights reserved.")]
[assembly: AssemblyVersionAttribute("2.0.0.0")]
[assembly: AssemblyInformationalVersionAttribute("2.0.0.909")]
[assembly: AssemblyFileVersionAttribute("2.0.0.0")]
