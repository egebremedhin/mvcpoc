using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Principal;
using Castle.Components.Validator;
using AreaAccessRight = Poc.Core.Enumerations.AreaAccessRight;

namespace Poc.Core.Entities.AdminModule
{
    public partial class AppUser : IPrincipal, IIdentity
    {
        public AppUser()
        {
            IsAuthenticated = false;
            IsActive = true;
            DateCreated = DateTime.Now;
            Roles = new HashSet<Role>();
        }
        
        public int? AreaAccessRightId { get; set; }
       
        public int AppUserId { get; set; }

        [ValidateNonEmpty("UserNameValidatorNonEmpty")]
        [ValidateLength(1, 50, "UserNameValidatorLength")]
        public string UserName { get; set; }

        [ValidateNonEmpty("PasswordValidatorNonEmpty")]
        [ValidateLength(5, 50, "PasswordValidatorLength")]
        public string Password { get; set; }

        [ValidateNonEmpty("PasswordConfirmationValidatorNonEmpty")]
        [ValidateLength(5, 50, "PasswordValidatorLength")]
        [ValidateSameAs("Password", "PasswordValidatorSameAs")]
        [NotMapped]
        public string PasswordConfirmation { get; set; }

        [ValidateNonEmpty("FirstNameValidatorNonEmpty")]
       [ValidateLength(1, 100, "FirstNameValidatorLength")]
        public string FirstName { get; set; }

        [ValidateNonEmpty("LastNameValidatorNonEmpty")]
        [ValidateLength(1, 100, "LastNameValidatorLength")]
        public string LastName { get; set; }

        public  string FullName
        {
            get
            {
                if (!string.IsNullOrEmpty(FirstName)
                    && !string.IsNullOrEmpty(LastName))
                {
                    return FirstName + " " + LastName;
                }
                return UserName;
            }
        }

        [ValidateNonEmpty("EmailValidatorNonEmpty")]
        [ValidateLength(1, 100, "EmailValidatorLength")]
        [ValidateEmail("EmailValidatorEmail")]
        public string Email { get; set; }

        public bool IsActive { get; set; }

        public DateTime? LastLogin { get; set; }

        public string LastIp { get; set; }

        public string AllowedPocAreas { get; set; }

        public string AllowedFacilitys { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime? DateModified { get; set; }
        
        //[ValidateCollectionNotEmpty("RolesValidatorNotEmpty")]
        public virtual ICollection<Role> Roles { get; set; }


        public AreaAccessRight AreaAccessRightToEnum
        {
            get
            {
                if (AreaAccessRightId.HasValue)
                    return (AreaAccessRight) Enum.ToObject(typeof (AreaAccessRight), AreaAccessRightId);
                return AreaAccessRight.Global;
            }
        }
        public static string HashPassword(string password)
        {
            return Util.Encryption.StringToMD5Hash(password);
        }

        public string GeneratePassword()
        {
            int length = 8;
            string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            string pwd = String.Empty;
            Random rnd = new Random();
            for (int i = 0; i < length; i++)
            {
                pwd += chars[rnd.Next(chars.Length)];
            }
            Password = AppUser.HashPassword(pwd);

            return pwd;
        }

        [NotMapped]
        public bool IsAuthenticated { get; set; }

        /// <summary>
        /// IIdentity property. 
        /// <remark>Returns a string with the Id of the user and not the username</remark>
        /// </summary>
        public string Name
        {
            get
            {
                if (IsAuthenticated)
                    return AppUserId.ToString();
                return "";
            }
        }

        /// <summary>
        /// IIdentity property <see cref="System.Security.Principal.IIdentity" />.
        /// </summary>
        public string AuthenticationType
        {
            get { return "PocAuthentication"; }
        }

        public bool IsInRole(string roleName)
        {
            return Roles.Any(role => role.Name == roleName);
        }

        public bool IsInRole(Role roleToCheck)
        {
            return Roles.Any(role => role.RoleId == roleToCheck.RoleId && role.Name == roleToCheck.Name);
        }

        public IIdentity Identity
        {
            get { return this; }
        }

        public bool CanView(Node node)
        {
            return node.Roles.Any(IsInRole);
        }
        public bool CanView(PocArea area)
        {
            var ids = GetAllowedAreaIds();
            return ids.Contains(area.Id);
        }
        public bool CanView(Facility facility)
        {
            if (AreaAccessRightId == 0)
                return true;
            
            if (AreaAccessRightId == 1)
            {
                return GetAllowedAreaIds().Contains(facility.AreaId1.Value);
            }
            if (AreaAccessRightId == 2)
            {
                if (facility.AreaId2.HasValue)
                    return GetAllowedAreaIds().Contains(facility.AreaId2.Value);
                return GetAllowedAreaIds().Contains(facility.PocAreaId);
            }
            if (AreaAccessRightId == 3)
            {
                return GetAllowedAreaIds().Contains(facility.PocAreaId);
            }
            var ids = GetAllowedFacilityIds();
            return ids.Contains(facility.FacilityId);
        }
        private IList<int> GetAllowedAreaIds()
        {
            if (AllowedPocAreas == null)
                return new List<int>();
            string temp = AllowedPocAreas.Replace('-', '|');
            var sids = temp.Split('|');
            if (sids.Length == 0)
                return new List<int>();

            return sids.Select(t => Int32.Parse(t)).ToList();
        }

        private IList<int> GetAllowedFacilityIds()
        {
            if (AllowedFacilitys == null)
                return new List<int>();
            var sids = AllowedFacilitys.Split('|');
            return sids.Select(t => Int32.Parse(t)).ToList();
        }

        public bool HasRight(string requiredRight)
        {
            return true;
        }

        public int? LevelOnePocAreaId
        {
            get
            {
                if (AllowedPocAreas == null)
                    return null;
                int i = AllowedPocAreas.IndexOf('-');
                if (i > 0)
                {
                    string temp = AllowedPocAreas.Substring(0, i);
                    return Convert.ToInt32(temp.Split('|')[0]);
                }
                return Convert.ToInt32(AllowedPocAreas.Split('|')[0]);
            }
        }

        public int? LevelTwoPocAreaId
        {
            get
            {
                 if (AllowedPocAreas == null)
                    return null;
                int i = AllowedPocAreas.IndexOf('-');
                if (i > 0)
                {
                    string temp = AllowedPocAreas.Substring(i + 1);
                    temp = temp.Replace('-', '|');
                    return Convert.ToInt32(temp.Split('|')[0]);
                }
                return null;
            }
        }

        public int? LevelThreePocAreaId
        {
            get
            {
                if (AllowedPocAreas == null)
                    return null;
                int count = AllowedPocAreas.Count(x=>x.Equals('-')); //LastIndexOf('-');
                if (count == 2)
                {
                    int i = AllowedPocAreas.LastIndexOf('-');
                    string temp = AllowedPocAreas.Substring(i+1);
                    //temp = temp.Replace('-', '|');
                    return Convert.ToInt32(temp.Split('|')[0]);
                }
                return null;
            }
        }

        public bool HasAdminRight()
        {
            return Roles.Any(role => role.IsSysAdmin);
        }
    }
}
