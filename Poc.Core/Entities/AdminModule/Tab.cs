using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Principal;
using Castle.Components.Validator;
using Poc.Core.Enumerations;


namespace Poc.Core.Entities.AdminModule
{
    public class Tab
    {
        public Tab()
        {
            Pages=new HashSet<Page>();
            Roles = new HashSet<Role>();
            Position = -1;
        }
        public int TabId { get; set; }

        [ValidateNonEmpty("TabNameValidatorNonEmpty")]
        [ValidateLength(1, 50, "TabNameValidatorLength")]
        public string TabName { get; set; }

        public int Position { get; set; }

        [ValidateNonEmpty("TabControllerNameValidatorNonEmpty")]
        [ValidateLength(1, 100, "TabControllerNameValidatorLength")]
        public string ControllerName { get; set; }
        
        public string Icon { get; set; }
        public virtual ICollection<Page> Pages { get; set; }
        public virtual ICollection<Role> Roles { get; set; }

        public List<int> GetAllNodeIds()
        {
            return Pages.Select(t => t.Node.NodeId).ToList();
        }

        public bool ViewAllowed(IIdentity user)
        {
            var pocUser = user as AppUser;
            if (pocUser != null)
            {
                return pocUser.Roles.Any(ViewAllowed);
            }

            return false;
        }

        public bool ViewAllowed(Role roleTocheck)
        {
            return Roles.Any(role => role.RoleId == roleTocheck.RoleId);
        }

        [NotMapped]
        public IList<Page> GetPopupTabNodes
        {
            get { return Pages.Where(x => x.MenuTypeToEnum == NodeMenuTypeEnum.TabPopup).ToList(); }
        }

        [NotMapped]
        public IList<Page> GetLeftSideTabNodes
        {
            get { return Pages.Where(x => x.MenuTypeToEnum == NodeMenuTypeEnum.LeftSide).ToList(); }
        }
    }
}
