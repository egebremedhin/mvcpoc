

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Poc.Core.Enumerations;

namespace Poc.Core.Entities.AdminModule
{
    public partial class Page
    {
        public Page()
        {
            IsActive = true;
        }
        public int PageId { get; set; }

        public int Position { get; set; }

        public bool IsActive { get; set; }
        public string MenuType { get; set; }
        public int NodeId { get; set; }
        public  Node Node { get; set; }
        public int TabId { get; set; }
        public Tab Tab { get; set; }

        public NodeMenuTypeEnum MenuTypeToEnum
        {
            get { return (NodeMenuTypeEnum)Enum.Parse(typeof (NodeMenuTypeEnum), MenuType); }
        }

        public void Move(IList<Page> rootNodes, NodePositionMovement npm)
        {
            switch (npm)
            {
                case NodePositionMovement.Up:
                    MoveUp(rootNodes);
                    break;
                case NodePositionMovement.Down:
                    MoveDown(rootNodes);
                    break;
            }
        }

        private void MoveUp(IList<Page> rootNodes)
        {
            if (Position > 0)
            {
                Position--;
                ((Page)rootNodes[Position]).Position++;
                //rootNodes.Remove(this);
                //rootNodes.Insert(Position, this);
            }
        }

        private void MoveDown(IList<Page> rootNodes)
        {
            if (Position < rootNodes.Count - 1)
            {
                this.Position++;
                ((Page)rootNodes[Position]).Position--;
                //rootNodes.Remove(this);
                //rootNodes.Insert(Position, this);
            }
        }
    }
}
