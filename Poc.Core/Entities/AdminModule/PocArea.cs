using Castle.Components.Validator;
using System.Collections.Generic;

namespace Poc.Core.Entities.AdminModule
{
    public partial class PocArea
    {
        public PocArea()
        {
            Facilitys = new HashSet<Facility>();
            //ChildPocAreas = new HashSet<PocArea>();
        }

        public int? ParentId { get; set; }

        public int AreaId { get; set; }

        public int Id { get; set; }

        [ValidateNonEmpty("PocAreaNameValidatorNonEmpty")]
        [ValidateLength(1, 50, "PocAreaNameValidatorLength")]
        public string Name { get; set; }

        public string ShortName { get; set; }

        public int Depth { get; set; }

        public string Code { get; set; }
        public string MapColor { get; set; }

        public string MapId { get; set; }

        public int? SortOrder { get; set; }

        public string AreaParentList { get; set; }

        public virtual Area Area { get; set; }

        public virtual ICollection<Facility> Facilitys { get; set; }

        //public virtual ICollection<PocArea> ChildPocAreas { get; set; }

        //public virtual PocArea ParentPocArea { get; set; }
    }
}
