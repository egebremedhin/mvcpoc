using Castle.Components.Validator;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Poc.Core.Entities.AdminModule
{

    [Table("Facilitys")]
    public partial class Facility
    {
        public int PocAreaId { get; set; }

        public int FacilityTypeId { get; set; }

        [Key]
        public int FacilityId { get; set; }

        [ValidateNonEmpty("FacilityNameValidatorNonEmpty")]
        [ValidateLength(1, 96, "FacilityNameValidatorLength")]
        public string FacilityName { get; set; }

        public string FacilityLevel { get; set; }

        public string FacilityCode { get; set; }

        public int? AreaId1 { get; set; }

        public int? AreaId2 { get; set; }

        public virtual PocArea PocArea { get; set; }
    }
}
