using System.Collections.Generic;
using Castle.Components.Validator;

namespace Poc.Core.Entities.AdminModule
{
    public partial class Role
    {
        public Role()
        {
            IsSysAdmin = false;
            AppUsers = new HashSet<AppUser>();
            Nodes = new HashSet<Node>();
            Tabs = new HashSet<Tab>();
        }
        public int RoleId { get; set; }

        [ValidateNonEmpty("RoleNameValidatorNonEmpty")]
        [ValidateLength(1, 32, "RoleNameValidatorLength")]
        public string Name { get; set; }

        public string Description { get; set; }
        
        public bool IsSysAdmin { get; set; }

        public virtual ICollection<AppUser> AppUsers { get; set; }

        public virtual ICollection<Node> Nodes { get; set; }

        public virtual ICollection<Tab> Tabs { get; set; }

    }
}
