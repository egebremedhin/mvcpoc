using System.Collections.Generic;
using Castle.Components.Validator;

namespace Poc.Core.Entities.AdminModule
{
    public partial class Area
    {
        public Area()
        {
            PocAreas = new HashSet<PocArea>();
        }
        public int Id { get; set; }

        [ValidateNonEmpty("AreaNameValidatorNonEmpty")]
        [ValidateLength(1, 50, "AreaNameValidatorLength")]
        public string Name { get; set; }

        public int Depth { get; set; }

        public virtual ICollection<PocArea> PocAreas { get; set; }
    }
}
