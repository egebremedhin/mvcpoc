using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
using System.Linq;
    using System.Security.Principal;
    using Castle.Components.Validator;

namespace Poc.Core.Entities.AdminModule
{
    public partial class Node
    {
        public Node()
        {
            Roles = new HashSet<Role>();
            Pages = new HashSet<Page>();
        }

        [Key]
        public int NodeId { get; set; }

        [ValidateNonEmpty("NodeNameValidatorNonEmpty")]
        [ValidateLength(1, 64, "NodeNameValidatorLength")]
        public string Name { get; set; }

        [ValidateLength(0, 400, "NodeDescriptionValidatorLength")]
        public string Description { get; set; }

        [ValidateNonEmpty("NodeActionNameValidatorNonEmpty")]
        [ValidateLength(1, 100, "NodeActionNameValidatorLength")]
        public string ActionName { get; set; }

        [ValidateNonEmpty("NodeControllerNameValidatorNonEmpty")]
        [ValidateLength(1, 100, "NodeControllerNameValidatorLength")]
        public string ControllerName { get; set; }

        public bool ShowInNavigation { get; set; }

        public virtual ICollection<Role> Roles { get; set; }
        public virtual ICollection<Page> Pages { get; set; } 
        public bool HasPermission(IIdentity user)
        {
            var pocUser = user as AppUser;
            return pocUser != null && pocUser.CanView(this);
        }

        public bool HasPermission(Role role)
        {
            return Roles.Any(np => np.RoleId == role.RoleId);
        }
    }
}
