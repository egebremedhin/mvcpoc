﻿namespace Poc.Core.Service
{
	/// <summary>
	/// Provides access to the current Poc context.
	/// </summary>
	public interface IPocContextProvider
	{
		/// <summary>
		/// Gets the Poc context (for the current request).
		/// </summary>
		/// <returns></returns>
		IPocContext GetContext();
	}
}
