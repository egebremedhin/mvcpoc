using Castle.MicroKernel;

namespace Poc.Core.Service
{
	/// <summary>
	/// Provides access to the current Poc context.
	/// </summary>
	public class PocContextProvider : IPocContextProvider
	{
		private IKernel _kernel;

		/// <summary>
		/// Creates a new instance of the <see cref="PocContextProvider"></see> class.
		/// </summary>
		/// <param name="kernel"></param>
		public PocContextProvider(IKernel kernel)
		{
			_kernel = kernel;
		}

		/// <summary>
		/// Gets the Poc context (for the current request).
		/// </summary>
		/// <returns></returns>
		public IPocContext GetContext()
		{
			return _kernel.Resolve<IPocContext>();
		}
	}
}