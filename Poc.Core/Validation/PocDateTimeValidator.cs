﻿using System.Security;
using Castle.Components.Validator;

namespace Poc.Core.Validation
{
	[SecurityCritical]
    public class PocDateTimeValidator : DateTimeValidator
	{
		public override void ApplyBrowserValidation(BrowserValidationConfiguration config, InputElementType inputType, IBrowserValidationGenerator generator, System.Collections.IDictionary attributes, string target)
		{
			base.ApplyBrowserValidation(config, inputType, generator, attributes, target);
			generator.SetDate(target, BuildErrorMessage());
		}
	}
}
