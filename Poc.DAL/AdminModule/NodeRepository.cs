using Poc.Core.Entities.AdminModule;
using Poc.DAL.Ambient;
using Poc.DAL.Contracts;
using Poc.DAL.Contracts.AdminModule;

namespace Poc.DAL.AdminModule
{
    public class NodeRepository : BaseRepository<Node>, INodeRepository
    {
        public NodeRepository(IAmbientDbContextLocator ambientDbContextLocator)
            : base(ambientDbContextLocator)
        {
        }
    }
}
