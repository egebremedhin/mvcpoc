﻿using System;
using System.Collections.Generic;
using System.Linq;
using Poc.Core.Entities.AdminModule;
using Poc.Core.Util;
using Poc.DAL.Ambient;
using Poc.DAL.Contracts;
using Poc.DAL.Contracts.AdminModule;

namespace Poc.DAL.AdminModule
{
    public class UserRepository : BaseRepository<AppUser>, IUserRepository
    {
        public UserRepository(IAmbientDbContextLocator ambientDbContextLocator): base(ambientDbContextLocator)
        {
        }

        public IList<AppUser> FindUsers(string username, int? role, bool? isActive, int pageSize, int pageNumber, out int totalCount)
        {
            var filter = new List<Filter>();
 
            
           if (!String.IsNullOrEmpty(username))
            {
                filter.Add(new Filter(){PropertyName = "UserName", Operation = Op.StartsWith,Value = username});
            }
           if (role != null)
           {
               filter.Add(new Filter() { PropertyName = "Roles", Operation = Op.Contains, Value = role });
           }
            if (isActive.HasValue)
            {
                filter.Add(new Filter() { PropertyName = "IsActive", Operation = Op.Equals, Value = isActive });
            }

            IEnumerable<AppUser> filteredCollection = null;
            if (filter.Count > 0)
            {
                var deleg = ExpressionBuilder.GetExpression<AppUser>(filter).Compile();
                filteredCollection = All().Where(deleg);
                //filteredCollection = role != null ? All().Where(deleg).Where(x => x.Roles.Contains(role)) : All().Where(deleg);
            }
            else
            {
                filteredCollection = All();// role != null ? All().Where(x => x.Roles.Contains(role)) : All();
            }
            var appUsers = filteredCollection as IList<AppUser> ?? filteredCollection.ToList();
            totalCount = appUsers.Count();

            return appUsers.Skip((pageNumber - 1)*pageSize).Take(pageSize).ToList();
        }

    }
}
