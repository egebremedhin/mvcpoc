using Poc.Core.Entities.AdminModule;
using Poc.DAL.Ambient;
using Poc.DAL.Contracts;
using Poc.DAL.Contracts.AdminModule;

namespace Poc.DAL.AdminModule
{
    public class RoleRepository : BaseRepository<Role>, IRoleRepository
    {
        public RoleRepository(IAmbientDbContextLocator ambientDbContextLocator)
            : base(ambientDbContextLocator)
        {
        }
    }
}
