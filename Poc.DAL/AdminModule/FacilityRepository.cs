﻿using Poc.Core.Entities.AdminModule;
using Poc.DAL.Ambient;
using Poc.DAL.Contracts;
using Poc.DAL.Contracts.AdminModule;

namespace Poc.DAL.AdminModule
{
    public class FacilityRepository : BaseRepository<Facility>, IFacilityRepository
    {
        public FacilityRepository(IAmbientDbContextLocator ambientDbContextLocator)
            : base(ambientDbContextLocator)
        {
        }
    }
}
