﻿using Poc.Core.Entities.AdminModule;
using Poc.DAL.Ambient;
using Poc.DAL.Contracts.AdminModule;

namespace Poc.DAL.AdminModule
{
    public class PocAreaRepository : BaseRepository<PocArea>, IPocAreaRepository
    {
        public PocAreaRepository(IAmbientDbContextLocator ambientDbContextLocator)
            : base(ambientDbContextLocator)
        {
        }
    }
}
