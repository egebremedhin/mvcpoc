﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

using Poc.DAL.Ambient;
using Poc.DAL.Contracts;

namespace Poc.DAL
{
    public class BaseRepository<T> : IBaseRepository<T>
        where T : class
    {
        //private readonly IPocUnitOfWork DbContext;
        private readonly IAmbientDbContextLocator _ambientDbContextLocator;

        private PocManagementDbContext DbContext
        {
            get
            {
                var dbContext = _ambientDbContextLocator.Get<PocManagementDbContext>();

                if (dbContext == null)
                    throw new InvalidOperationException(
                        "No ambient DbContext of type PocManagementDbContext found. This means that this repository method has been called outside of the scope of a DbContextScope. A repository must only be accessed within the scope of a DbContextScope, which takes care of creating the DbContext instances that the repositories need and making them available as ambient contexts. This is what ensures that, for any given DbContext-derived type, the same instance is used throughout the duration of a business transaction. To fix this issue, use IDbContextScopeFactory in your top-level business logic service method to create a DbContextScope that wraps the entire business transaction that your service method implements. Then access this repository within that scope. Refer to the comments in the IDbContextScope.cs file for more details.");

                return dbContext;
            }
        }

        public BaseRepository(IAmbientDbContextLocator ambientDbContextLocator)
        {
            if (ambientDbContextLocator == null)
                throw new ArgumentNullException("ambientDbContextLocator");
            _ambientDbContextLocator = ambientDbContextLocator;
        }

        #region Private Methods

        private IDbSet<T> GetSet()
        {
            return DbContext.CreateSet<T>();
        }

        #endregion

        #region IRepository Members

        public void Add(T item)
        {
            if (item != (T) null)
                GetSet().Add(item); // add new item in this set
            else
            {
                throw new Exception(String.Format("Cannot add null entity into {0} repository", typeof (T)));
            }
        }

        public void Update(T item)
        {
            if (item != (T) null)
                DbContext.SetModified(item);
            else
            {
                throw new Exception(String.Format("Cannot modify null item into {0} repository", typeof (T).ToString()));
            }
        }

        public void Delete(T item)
        {
            if (item != (T) null)
            {
                //attach item if not exist
                DbContext.Attach(item);

                //set as "removed"
                GetSet().Remove(item);
            }
            else
            {
                throw new Exception(String.Format("Cannot remove null entity into {0} repository", typeof (T).ToString()));
            }
        }

        public void Delete(Expression<Func<T, bool>> expression)
        {
            foreach (var item in GetSet().Where(expression))
                Delete(item);
        }

        public T Get(int id)
        {
            if (id != 0)
                return GetSet().Find(id);
            else
                return null;
        }

        public T Get(Expression<Func<T, bool>> expression)
        {
            return GetSet().SingleOrDefault(expression);
        }


        public T Get(Expression<Func<T, bool>> expression, params Expression<Func<T, object>>[] includes)
        {
            if (includes == null || includes.Length < 1)
                return DbContext.Trackable<T>().Where(expression).SingleOrDefault();
            var result =
                includes.Aggregate(DbContext.Trackable<T>(), (current, include) => current.Include(include))
                    .Where(expression);
            return result.SingleOrDefault();
        }

        public IEnumerable<T> All()
        {
            return GetSet();
        }

        public IEnumerable<T> All(params Expression<Func<T, object>>[] includes)
        {
           return includes.Aggregate(DbContext.Trackable<T>(), (current, include) => current.Include(include));
        }

        public IEnumerable<T> All(Expression<Func<T, bool>> expression)
        {
            return GetSet().Where(expression);
        }

        public IEnumerable<T> GetPaged<TKProperty>(int pageIndex, int pageCount,
            Expression<Func<T, TKProperty>> orderByExpression, bool ascending)
        {
            var set = GetSet();

            if (ascending)
            {
                return set.OrderBy(orderByExpression)
                    .Skip(pageCount*pageIndex)
                    .Take(pageCount);
            }
            else
            {
                return set.OrderByDescending(orderByExpression)
                    .Skip(pageCount*pageIndex)
                    .Take(pageCount);
            }
        }

        public IEnumerable<T> Query(Expression<Func<T, bool>> predictate, params Expression<Func<T, object>>[] includes)
        {
            if (includes != null && predictate != null)
                return
                    includes.Aggregate(DbContext.Trackable<T>(), (current, include) => current.Include(include))
                        .Where(predictate);
            if (includes != null)
                return includes.Aggregate(DbContext.Trackable<T>(), (current, include) => current.Include(include));
            if (predictate != null)
                return DbContext.Trackable<T>().Where(predictate);
            return DbContext.Trackable<T>();
        }

        public IEnumerable<string> Distinct(Expression<Func<T, string>> expression)
        {
            return DbContext.Trackable<T>().Select(expression).Distinct().Where(x => !string.IsNullOrEmpty(x)).ToList();
        }

        public IEnumerable<TResult> Select<TResult>(Expression<Func<T, TResult>> expression,
            Expression<Func<T, bool>> predictate)
        {
            if (predictate != null)
                return DbContext.Trackable<T>().Where(predictate).Select(expression);
            return DbContext.Trackable<T>().Select(expression);
        }

        public int Count(Expression<Func<T, bool>> predictate)
        {
            if (predictate != null)
                return DbContext.Trackable<T>().Count(predictate);
            return DbContext.Trackable<T>().Count();
        }

        public int Count()
        {
            return DbContext.Trackable<T>().Count();
        }

        public IQueryable<T> Queryable()
        {
            return DbContext.Trackable<T>();
        }

        #endregion


        public void ApplyCurrentValues(T original, T current)
        {
            //if it is not attached, attach original and set current values
            DbContext.Entry<T>(original).CurrentValues.SetValues(current);
        }

    }
}
