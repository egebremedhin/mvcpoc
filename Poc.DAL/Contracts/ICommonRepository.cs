﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Poc.DAL.Contracts
{
    public interface ICommonRepository
    {
        int Count<T>() where T : class;

        T Get<T>(int id) where T : class;
        T Single<T>(Expression<Func<T, bool>> expression) where T : class;
        T Single<T>(Expression<Func<T, bool>> predictate, string[] includes) where T : class;
        T Single<T>(Expression<Func<T, bool>> expression, params Expression<Func<T, object>>[] includes) where T : class;
       
        IEnumerable<T> All<T>() where T : class;
        IEnumerable<T> All<T>(params Expression<Func<T, object>>[] includes) where T : class;
        IEnumerable<T> All<T>(Expression<Func<T, bool>> expression) where T : class;

        void Add<T>(T item) where T : class;
        void Add<T>(IEnumerable<T> items) where T : class;
        void Update<T>(T item) where T : class;
        void Delete<T>(Expression<Func<T, bool>> expression) where T : class;
        void Delete<T>(T item) where T : class;
        void DeleteAll<T>() where T : class;

        IEnumerable<T> Query<T>(Expression<Func<T, bool>> predictate, params Expression<Func<T, object>>[] includes)where T : class;

        IEnumerable<string> Distinct<T>(Expression<Func<T, string>> expression) where T : class;

        IEnumerable<TResult> Select<TSource, TResult>(Expression<Func<TSource, TResult>> expression,Expression<Func<TSource, bool>> predictate) where TSource : class;

        int Count<T>(Expression<Func<T, bool>> predictate) where T : class;
        IQueryable<T> Queryable<T>() where T : class;
       
    }
}
