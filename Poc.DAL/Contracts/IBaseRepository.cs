﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Poc.DAL.Contracts
{
    /// <summary>
    /// Base interface to implement Repository Pattern
    /// </remarks>
    /// <typeparam name="T">Type of entity for this repository </typeparam>
    public interface IBaseRepository<T> : IBaseRepository where T : class
    {

        /// <summary>
        /// Add item into repository
        /// </summary>
        /// <param name="item">Item to add to repository</param>
        void Add(T item);

        /// <summary>
        /// Delete item 
        /// </summary>
        /// <param name="item">Item to delete</param>
        void Delete(T item);

        void Delete(Expression<Func<T, bool>> expression);

        /// <summary>
        /// Set item as modified
        /// </summary>
        /// <param name="item">Item to modify</param>
        void Update(T item);

        void ApplyCurrentValues(T original, T current);
        
        /// <summary>
        /// Get element by entity key
        /// </summary>
        /// <param name="id">Entity key value</param>
        /// <returns></returns>
        T Get(int id);
        T Get(Expression<Func<T, bool>> expression);
        T Get(Expression<Func<T, bool>> expression, params Expression<Func<T, object>>[] includes);

        /// <summary>
        /// Get all elements of type T in repository
        /// </summary>
        /// <returns>List of selected elements</returns>
        IEnumerable<T> All();

        IEnumerable<T> All(params Expression<Func<T, object>>[] includes);
        IEnumerable<T> All(Expression<Func<T, bool>> expression);

        /// <summary>
        /// Get all elements of type T in repository
        /// </summary>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageCount">Number of elements in each page</param>
        /// <param name="orderByExpression">Order by expression for this query</param>
        /// <param name="ascending">Specify if order is ascending</param>
        /// <returns>List of selected elements</returns>
        IEnumerable<T> GetPaged<Property>(int pageIndex, int pageCount, Expression<Func<T, Property>> orderByExpression,
            bool ascending);

        IEnumerable<T> Query(Expression<Func<T, bool>> predictate, params Expression<Func<T, object>>[] includes);

        IEnumerable<string> Distinct(Expression<Func<T, string>> expression);

        IEnumerable<TResult> Select<TResult>(Expression<Func<T, TResult>> expression,
            Expression<Func<T, bool>> predictate);

        int Count(Expression<Func<T, bool>> predictate);
        IQueryable<T> Queryable();
    }

    public interface IBaseRepository 
    {
        
    }
}
