﻿using System.Collections.Generic;
using Poc.Core.Entities.AdminModule;

namespace Poc.DAL.Contracts.AdminModule
{
    public interface IUserRepository : IBaseRepository<AppUser>
    {
        IList<AppUser> FindUsers(string username, int? roleId, bool? isActive, int pageSize, int pageNumber,
            out int totalCount);
    }
}
