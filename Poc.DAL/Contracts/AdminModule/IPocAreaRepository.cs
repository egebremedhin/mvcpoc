﻿using Poc.Core.Entities.AdminModule;

namespace Poc.DAL.Contracts.AdminModule
{
    public interface IPocAreaRepository : IBaseRepository<PocArea>
    {
    }
}
