using Poc.Core.Entities.AdminModule;

namespace Poc.DAL.Contracts.AdminModule
{
    public interface IRoleRepository : IBaseRepository<Role>
    {
    }
}
