using Poc.Core.Entities.AdminModule;

namespace Poc.DAL.Contracts.AdminModule
{
    public interface INodeRepository : IBaseRepository<Node>
    {
    }
}
