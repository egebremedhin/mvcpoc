﻿using Poc.Core.Entities.AdminModule;

namespace Poc.DAL.Contracts.AdminModule
{
    public interface ITabRepository : IBaseRepository<Tab>
    {
    }
}
