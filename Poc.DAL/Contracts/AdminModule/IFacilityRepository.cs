﻿using Poc.Core.Entities.AdminModule;

namespace Poc.DAL.Contracts.AdminModule
{
    public interface IFacilityRepository : IBaseRepository<Facility>
    {
    }
}
