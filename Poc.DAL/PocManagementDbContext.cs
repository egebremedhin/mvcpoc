﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;

using Poc.Core.Entities.AdminModule;
using Poc.Core.Util;

namespace Poc.DAL
{
    public class PocManagementDbContext : DbContext
    {
         private static readonly string ConnString = Config.GetConfiguration()["ConnectionString"];
        public PocManagementDbContext()
            : base(ConnString)
        {
            Database.SetInitializer<PocManagementDbContext>(null);
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
        }

        #region IPocUnitOfWork Members
        
        public DbSet<T> CreateSet<T>()
            where T : class
        {
            return base.Set<T>();
        }
        public void SetModified<T>(T item)
            where T : class
        {
            //this operation also attach item in object state manager
            base.Entry<T>(item).State = EntityState.Modified;
        }
        public void Attach<T>(T item)
            where T : class
        {
            //attach and set as unchanged
            base.Entry<T>(item).State = EntityState.Unchanged;
        }
        public IQueryable<T> Trackable<T>() where T : class
        {
            return base.Set<T>();
            //return ObjContext().CreateObjectSet<T>();
        }
        public void ApplyCurrentValues<T>(T original, T current) where T : class
        {
            //if it is not attached, attach original and set current values
            base.Entry<T>(original).CurrentValues.SetValues(current);
        }
        public IEnumerable<T> ExecuteQuery<T>(string sqlQuery, params object[] parameters)
        {
            return base.Database.SqlQuery<T>(sqlQuery, parameters);
        }

        public int ExecuteCommand(string sqlCommand, params object[] parameters)
        {
            return base.Database.ExecuteSqlCommand(sqlCommand, parameters);
        }

        public ObjectContext ObjContext()
        {
            return ((IObjectContextAdapter)this).ObjectContext;
        }

        #endregion

        public virtual DbSet<AppUser> AppUsers { get; set; }
        public virtual DbSet<Area> Areas { get; set; }
        public virtual DbSet<Facility> Facilitys { get; set; }
        public virtual DbSet<Node> Nodes { get; set; }
        public virtual DbSet<PocArea> PocAreas { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<Tab> Tabs { get; set; }
        public virtual DbSet<Page> Pages { get; set; }

        #region DbContext Overrides

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Remove unused conventions
            //modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            modelBuilder.Entity<AppUser>()
                .HasMany(e => e.Roles)
                .WithMany(e => e.AppUsers)
                .Map(m => m.ToTable("AppUserRoles").MapLeftKey("AppUserId").MapRightKey("RoleId"));

            modelBuilder.Entity<Area>()
                .HasMany(e => e.PocAreas)
                .WithRequired(e => e.Area)
                .WillCascadeOnDelete(false);

            //modelBuilder.Entity<Node>()
            //    .HasMany(e => e.Tabnodes)
            //    .WithRequired(e => e.Node)
            //    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Node>()
                .HasMany(e => e.Roles)
                .WithMany(e => e.Nodes)
                .Map(m => m.ToTable("NodeRoles").MapLeftKey("NodeId").MapRightKey("RoleId"));

            modelBuilder.Entity<PocArea>()
                .HasMany(e => e.Facilitys)
                .WithRequired(e => e.PocArea)
                .WillCascadeOnDelete(false);

            //modelBuilder.Entity<PocArea>()
            //    .HasMany(e => e.ChildPocAreas)
            //    .WithOptional(e => e.ParentPocArea)
            //    .HasForeignKey(e => e.ParentId);

            modelBuilder.Entity<Role>()
                .HasMany(e => e.Tabs)
                .WithMany(e => e.Roles)
                .Map(m => m.ToTable("TabRoles").MapLeftKey("RoleId").MapRightKey("TabId"));

            modelBuilder.Entity<Tab>()
                .HasMany(e => e.Pages)
            .WithRequired(e => e.Tab)
                .HasForeignKey(e => e.TabId);


            //modelBuilder.Entity<Tab>()
            //  .HasMany(e => e.Roles)
            //  .WithMany(e => e.Tabs)
            //  .Map(m => m.ToTable("TabRoles").MapLeftKey("TabId").MapRightKey("RoleId"));

            modelBuilder.Entity<Page>()
                .HasRequired(e => e.Tab)
                .WithMany(e => e.Pages)
                .HasForeignKey(e => e.TabId);

            modelBuilder.Entity<Node>()
                .HasMany(e => e.Pages)
                .WithRequired(e => e.Node)
                .HasForeignKey(e => e.NodeId);

            base.OnModelCreating(modelBuilder);

        }

        #endregion
    }
}
