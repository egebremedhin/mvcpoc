﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using Poc.DAL.Ambient;
using Poc.DAL.Contracts;

namespace Poc.DAL
{
    public class CommonRepository : ICommonRepository
    {

        private readonly IAmbientDbContextLocator _ambientDbContextLocator;
       
        public CommonRepository(IAmbientDbContextLocator ambientDbContextLocator)
        {
            _ambientDbContextLocator = ambientDbContextLocator;
        }

        private PocManagementDbContext DbContext
        {
            get
            {
                var dbContext = _ambientDbContextLocator.Get<PocManagementDbContext>();

                if (dbContext == null)
                    throw new InvalidOperationException(
                        "No ambient DbContext of type PocManagementDbContext found. This means that this repository method has been called outside of the scope of a DbContextScope. A repository must only be accessed within the scope of a DbContextScope, which takes care of creating the DbContext instances that the repositories need and making them available as ambient contexts. This is what ensures that, for any given DbContext-derived type, the same instance is used throughout the duration of a business transaction. To fix this issue, use IDbContextScopeFactory in your top-level business logic service method to create a DbContextScope that wraps the entire business transaction that your service method implements. Then access this repository within that scope. Refer to the comments in the IDbContextScope.cs file for more details.");

                return dbContext;
            }
        }
        #region Private Methods

        private IDbSet<T> GetSet<T>() where T : class
        {
            return DbContext.CreateSet<T>();
        }

        #endregion

        #region IRepository Members

        public virtual void TrackItem<T>(T item) where T : class
        {
            if (item != (T) null)
                DbContext.Attach<T>(item);
            else
            {
                throw new Exception(String.Format("Cannot track null item into {0} repository", typeof (T).ToString()));
            }
        }

        public virtual void Merge<T>(T persisted, T current) where T : class
        {
            DbContext.ApplyCurrentValues(persisted, current);
        }

        public virtual void Add<T>(T item) where T : class
        {
            if (item != (T) null)
                GetSet<T>().Add(item); // add new item in this set
            else
            {
                throw new Exception(String.Format("Cannot add null entity into {0} repository", typeof (T).ToString()));
            }
        }

        public void Add<T>(IEnumerable<T> items) where T : class
        {
            foreach (var item in items)
                Add(item);
        }

        public virtual void Update<T>(T item) where T : class
        {
            if (item != (T) null)
                DbContext.SetModified(item);
            else
            {
                throw new Exception(String.Format("Cannot modify null item into {0} repository", typeof (T).ToString()));
            }
        }

        public virtual void Delete<T>(T item) where T : class
        {
            if (item != (T) null)
            {
                //attach item if not exist
                DbContext.Attach(item);

                //set as "removed"
                GetSet<T>().Remove(item);
            }
            else
            {
                throw new Exception(String.Format("Cannot remove null entity into {0} repository", typeof (T).ToString()));
            }
        }

        public void Delete<T>(Expression<Func<T, bool>> expression) where T : class
        {
            foreach (var item in GetSet<T>().Where(expression))
                Delete(item);
        }

        public void DeleteAll<T>() where T : class
        {
            foreach (var item in GetSet<T>())
                Delete(item);
        }

        public virtual T Get<T>(int id) where T : class
        {
            if (id != 0)
                return GetSet<T>().Find(id);
            else
                return null;
        }

        public virtual IEnumerable<T> All<T>() where T : class
        {
            return GetSet<T>();
        }

        public virtual IEnumerable<T> All<T>(params Expression<Func<T, object>>[] includes) where T : class
        {
            return includes.Aggregate(DbContext.Trackable<T>(), (current, include) => current.Include(include));
        }

        public virtual IEnumerable<T> All<T>(Expression<Func<T, bool>> expression) where T : class
        {
            return GetSet<T>().Where(expression);
        }

        public IEnumerable<T> Query<T>(Expression<Func<T, bool>> predictate,
            params Expression<Func<T, object>>[] includes) where T : class
        {
            if (includes != null && predictate != null)
                return
                    includes.Aggregate(DbContext.Trackable<T>(), (current, include) => current.Include(include))
                        .Where(predictate);
            if (includes != null)
                return includes.Aggregate(DbContext.Trackable<T>(), (current, include) => current.Include(include));
            if (predictate != null)
                return DbContext.Trackable<T>().Where(predictate);
            return DbContext.Trackable<T>();
        }

        public IEnumerable<string> Distinct<T>(Expression<Func<T, string>> expression) where T : class
        {
            return DbContext.Trackable<T>().Select(expression).Distinct().Where(x => !string.IsNullOrEmpty(x)).ToList();
        }

        public T Single<T>(Expression<Func<T, bool>> expression) where T : class
        {
            return DbContext.Trackable<T>().SingleOrDefault(expression);
        }

        public T Single<T>(Expression<Func<T, bool>> expression, string[] includes) where T : class
        {
            if (includes == null || includes.Length < 1)
                return DbContext.Trackable<T>().Where(expression).SingleOrDefault();

            return
                includes.Aggregate(DbContext.Trackable<T>() as ObjectQuery<T>,
                    (current, include) => current.Include(include)).Where(expression).SingleOrDefault();
            ;
        }

        public T Single<T>(Expression<Func<T, bool>> predictate, params Expression<Func<T, object>>[] includes)
            where T : class
        {
            return
                includes.Aggregate(DbContext.Trackable<T>(), (current, include) => current.Include(include))
                    .Where(predictate)
                    .SingleOrDefault();
        }

        public IEnumerable<TResult> Select<TSource, TResult>(Expression<Func<TSource, TResult>> expression,
            Expression<Func<TSource, bool>> predictate) where TSource : class
        {
            if (predictate != null)
                return DbContext.Trackable<TSource>().Where(predictate).Select(expression);
            return DbContext.Trackable<TSource>().Select(expression);
        }

        public int Count<T>(Expression<Func<T, bool>> predictate) where T : class
        {
            if (predictate != null)
                return DbContext.Trackable<T>().Count(predictate);
            return DbContext.Trackable<T>().Count();
        }

        public int Count<T>() where T : class
        {
            return DbContext.Trackable<T>().Count();
        }

        public IQueryable<T> Queryable<T>() where T : class
        {
            return DbContext.Trackable<T>();
        }

        public DataTable QueryToTable(string queryText, CommandType commandType, SqlParameter[] parametes)
        {
            using (DbDataAdapter adapter = new SqlDataAdapter())
            {
                adapter.SelectCommand = DbContext.Database.Connection.CreateCommand();
                adapter.SelectCommand.CommandText = queryText;
                adapter.SelectCommand.CommandType = commandType;
                if (parametes != null)
                    adapter.SelectCommand.Parameters.AddRange(parametes);

                var table = new DataTable();
                adapter.Fill(table);
                return table;
            }
        }

        #endregion
        
    }
}