﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using Poc.Core.Entities.AdminModule;
using Poc.Core.Validation;
using Poc.Service.AdminModule;

namespace Poc.Web.Controllers
{
    public class NodesController : ManagerController
    {
        private readonly INodeService _nodeService;
        private readonly IUserService _userService;
        public NodesController(INodeService nodeService,IUserService userService, IModelValidator<Node> modelValidator)
		{
			_nodeService = nodeService;
            _userService = userService;
			ModelValidator = modelValidator;
		}
        //
        // GET: /Nodes/

        public ActionResult Index()
        {
            ViewData["Roles"] = _userService.GetAllRoles();
            var nodes = _nodeService.GetAllNodes();
            return View("Index",nodes);
        }

       
        //
        // GET: /Nodes/Create

        public ActionResult Create()
        {
            ViewData["Roles"] = _userService.GetAllRoles();
            var node = new Node();
            return View("Create", node);
        }

        //
        // POST: /Nodes/Create

        [HttpPost]
        public ActionResult Create(int[] roleIds)
        {
            var newNode = new Node();
            var roles = _userService.GetAllRoles();
            //try
            //{
                UpdateModel(newNode, new[] {"Name", "Description", "ControllerName", "ActionName", "ShowInNavigation"});
                //_nodeService.SetNodePermissions(newNode,RoleIds);
                if (ValidateModel(newNode))
                {
                    _nodeService.SaveNode(newNode,roleIds);
                    Messages.AddFlashMessageWithParams("NodeCreatedMessage", newNode.Name);
                    return RedirectToAction("Index");
                }
            //}
            //catch (Exception ex)
            //{
            //    Messages.AddException(ex);
            //}

            ViewData["Roles"] = roles;

            return View("Create", newNode);
        }

        //
        // GET: /Nodes/Edit/5

        public ActionResult Edit(int id)
        {
            ViewData["Roles"] = _userService.GetAllRoles();
            var node = _nodeService.GetNodeById(id);
            return View("Edit",node);
        }

        //
        // POST: /Nodes/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, int[] roleIds)
        {
            var node = _nodeService.GetNodeById(id);
            var roles = _userService.GetAllRoles();
            try
            {
                UpdateModel(node, new[] { "Name", "Description", "ControllerName", "ActionName", "ShowInNavigation" });
                
                if (ValidateModel(node))
                {
                    _nodeService.UpdateNode(node,roleIds);
                    Messages.AddFlashMessageWithParams("NodeCreatedMessage", node.Name);
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                Messages.AddException(ex);
            }

            ViewData["Roles"] = roles;

            return View("Edit", node);
        }

        //
        // POST: /Nodes/Delete/5

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var node = _nodeService.GetNodeById(id);
            try
            {
                _nodeService.DeleteNode(node);
                Messages.AddFlashMessageWithParams("NodeDeletedMessage", node.Name);
            }
            catch(Exception ex)
            {
                Messages.AddFlashException(ex);
            }
            return RedirectToAction("Index");
        }


        public ActionResult PermissionsImport()
        {
            var controllerTypes = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(a => a.GetTypes())
                .Where(t => t != null
                    && t.IsPublic
                    && t.Name.EndsWith("Controller", StringComparison.OrdinalIgnoreCase)
                    && !t.IsAbstract
                    && typeof(IController).IsAssignableFrom(t));

            var controllerMethods = controllerTypes.ToDictionary(controllerType => controllerType,
                    controllerType => controllerType.GetMethods(BindingFlags.Public | BindingFlags.Instance)
                    .Where(m => typeof(ActionResult).IsAssignableFrom(m.ReturnType)));

            foreach (var controller in controllerMethods)
            {
                string controllerName = controller.Key.Name;
                foreach (var controllerAction in controller.Value)
                {
                    string controllerActionName = controllerAction.Name;
                    if (controllerName.EndsWith("Controller"))
                    {
                        controllerName = controllerName.Substring(0, controllerName.LastIndexOf("Controller"));
                    }

                    string permissionDescription = string.Format("{0}-{1}", controllerName, controllerActionName);
                    Node node = _nodeService.GetNode(controllerName, controllerActionName);
                    
                    if (node == null)
                    {
                        if (ModelState.IsValid)
                        {
                            var newNode = new Node()
                            {
                                Name = permissionDescription,
                                ControllerName = controllerName,
                                ActionName =controllerActionName,
                                ShowInNavigation = true
                            };
                            _nodeService.SaveNode(newNode, null);
                        }
                    }
                }
            }
            return RedirectToAction("Index");
        }
    }
}
