using System.Collections.Generic;
using Poc.Core.Entities.AdminModule;
using Poc.Service.AdminModule;
using Poc.Web.Filters;
using Poc.Web.Mvc.Controllers;

namespace Poc.Web.Controllers
{
    [MenuDataFilter]
	 public class ManagerController : SecureController
    {
        private ITabService _tabService;
        public ITabService TabService
        {
            set { _tabService = value; }
        }
        protected override void OnActionExecuting(System.Web.Mvc.ActionExecutingContext filterContext)
        {

            ViewData["PocTabs"] = _tabService.GetAllTabs();

            base.OnActionExecuting(filterContext);
        }
    }
}
