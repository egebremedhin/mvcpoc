﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Poc.Core.Entities.AdminModule;
using Poc.Core.Enumerations;
using Poc.Core.Validation;
using Poc.Service.AdminModule;

namespace Poc.Web.Controllers
{
    public class TabsController : ManagerController
    {
        private readonly ITabService _tabService;
        private readonly IUserService _userService;
        private readonly INodeService _nodeService;
        public TabsController(ITabService tabService, IUserService userService, INodeService nodeService, IModelValidator<Tab> modelValidator)
        {
            _tabService = tabService;
            _userService = userService;
            _nodeService = nodeService;
            ModelValidator = modelValidator;
        }
        public ActionResult Index()
        {
            return View();
        }
        
        //
        // GET: /Tabs/Create

        public ActionResult Create()
        {
            ViewData["Roles"] = _userService.GetAllRoles();
            var newTab = new Tab();
            return View("Create",newTab);
        }

        //
        // POST: /Tabs/Create

        [HttpPost]
        public ActionResult Create(int[] roleIds)
        {
            var newTab = new Tab();
          
            try
            {
                if (TryUpdateModel(newTab, new[] {"TabName", "ControllerName"}))
                {
                    if (ValidateModel(newTab))
                    {
                        _tabService.CreateTab(newTab, roleIds);
                        Messages.AddFlashMessageWithParams("PageCreatedMessage", newTab.TabName);

                        return RedirectToAction("Edit", new {id = newTab.TabId});
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.AddException(ex);
            }

            ViewData["Roles"] = _userService.GetAllRoles();
            return View("Create", newTab);
        }

        //
        // GET: /Tabs/Edit/5

        public ActionResult Edit(int id)
        {
            ViewData["Roles"] = _userService.GetAllRoles();
            var tab = _tabService.GetTabById(id);
            return View("Edit",tab);
        }

        //
        // POST: /Tabs/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, int[] roleIds)
        {
            var tab = _tabService.GetTabById(id);
            
            try
            {
                if (TryUpdateModel(tab, new[] {"TabName", "ControllerName"}))
                {
                    if (ValidateModel(tab))
                    {
                        _tabService.UpdateTab(tab, roleIds);
                        Messages.AddFlashMessage("PagePropertiesUpdatedMessage");
                    }
                }
            }
            catch (Exception ex)
            {
                Messages.AddException(ex);
            }
            ViewData["Roles"] = _userService.GetAllRoles();
            return View("Edit", _tabService.GetTabById(id));
        }

        //
        // GET: /Tabs/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Tabs/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult MoveNode(int id, int nodeid, string direction)
        {
            try
            {
                var direc = (NodePositionMovement) Enum.Parse(typeof (NodePositionMovement), direction, true);
                _tabService.MovePage(id, nodeid, direc);
                Messages.AddFlashMessage("PageMovedMessage");
            }
            catch (Exception ex)
            {
                Messages.AddException(ex);
            }
            ViewData["Roles"] = _userService.GetAllRoles();
            var tab = _tabService.GetTabById(id);
            return View("Edit", tab);
        }

        public ActionResult DeleteTabNode(int id, int nodeid)
        {   
             try
             {
                 _tabService.DeletePage(id, nodeid);
                Messages.AddFlashMessage("NodeDeletedMessage");
            }
            catch (Exception ex)
            {
                Messages.AddException(ex);
            }

            ViewData["Roles"] = _userService.GetAllRoles();
            var tab = _tabService.GetTabById(id);
            return View("Edit", tab);
        }

        public ActionResult SelectTabNode(int id)
        {
            var tab = _tabService.GetTabById(id);
            var nodes = _nodeService.GetAllNodes();
            IList<Node> filiterd = nodes.Where(node => !tab.GetAllNodeIds().Contains(node.NodeId)).ToList();
            ViewData["tabId"] = tab.TabId;

            return View("_SelectNode", filiterd);
        }

        [HttpPost]
        public ActionResult SelectTabNode(int id,int[] nodeIds)
        {
            if (nodeIds != null && nodeIds.Length > 0)
            {
                try
                {
                    var pages = new List<Page>();
                    int len = nodeIds.Length;
                    int pos = _tabService.GetPageCount(id);
                    for (int i = 0; i < len; i++)
                    {
                        var tabNode = new Page
                        {
                            IsActive = true,
                            MenuType = NodeMenuTypeEnum.LeftSide.ToString(),
                            NodeId =  nodeIds[i],
                            TabId = id,
                            Position = pos
                        };
                        pos++;
                        pages.Add(tabNode);
                    }

                    _tabService.UpdateTab(id,pages);
                    Messages.AddFlashMessage("PagePropertiesUpdatedMessage");
                }
                catch (Exception ex)
                {
                    Messages.AddException(ex);
                }
            }

            var tab = _tabService.GetTabById(id); 
            ViewData["Roles"] = _userService.GetAllRoles();
            return View("Edit", tab);
        }
    }
}
