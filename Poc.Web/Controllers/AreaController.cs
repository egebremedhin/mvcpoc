﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Poc.Core.Entities.AdminModule;
using Poc.Core.Validation;
using Poc.Service.AdminModule;

namespace Poc.Web.Controllers
{
    public class AreaController : ManagerController
    {
        private readonly IFacilityService _facilityService;
        private IModelValidator<Area> _areaModelValidator;
        public AreaController(IFacilityService facilityService, IModelValidator<PocArea> modelValidator, IModelValidator<Area> areaModelValidator)
		{
			_facilityService = facilityService;
			ModelValidator = modelValidator;
            _areaModelValidator = areaModelValidator;
		}
        public ActionResult Index()
        {
            return View("Index");
        }

        public ActionResult Areas()
        {
            var areas = _facilityService.GetAllAreas();
            var level1 = _facilityService.GetAreaByDepth(1);
            if (level1 != null)
                ViewData["Level1AreaName"] = level1.Name +"s";
            else
                ViewData["Level1AreaName"] = "";
            ViewData["PocAreas"] = _facilityService.GetRootPocArea();
            return View("Areas", areas);
        }
        public ActionResult NewArea()
        {
            var area = new Area();

            return PartialView("NewArea", area);
        }
        
        public ActionResult EditArea(int id)
        {
            var area = _facilityService.GetAreaById(id);
            
            return PartialView("EditArea", area);
        }

        [HttpPost]
        public ActionResult EditArea(int id,FormCollection collection)
        {
            var depth = _facilityService.GetDepthOfArea();
            var area = new Area();
            if (id > 0)
                area = _facilityService.GetAreaById(id);
            else
            {
                area.Depth = depth + 1;
            }

            if (TryUpdateModel(area, new[] {"Name"}))
            {
                if (ValidateModel(area,_areaModelValidator,new []{"Name"}))
                {
                    if (id > 0)
                        _facilityService.UpdateArea(area);
                    else
                        _facilityService.CreateArea(area);

                    return Json(new {success = true});
                }
            }

            return PartialView("EditArea", area);
        }

        [HttpPost]
        public ActionResult DeleteArea(int id)
        {
            var area = _facilityService.GetAreaById(id);
            try
            {
               _facilityService.DeleteArea(area);

               Messages.AddFlashMessageWithParams("AreaDeletedMessage", area.Name);
            }
            catch (Exception ex)
            {
                Messages.AddFlashException(ex);
            }

            return RedirectToAction("Index");
        }

        public ActionResult NewPocArea(int? parentid)
        {
            var depth = 1;
            var parea = new PocArea();
            PocArea parent = null;
            if (parentid.HasValue && parentid.Value > 0)
            {
                parent = _facilityService.GetPocAreaById(parentid.Value);
                depth = parent.Area.Depth + 1;
                parea.ParentId = parentid.Value;
            }
            var area = _facilityService.GetAreaByDepth(depth);
            parea.Area = area;
            
            ViewData["ParentPocArea"] = parent;
            return View("NewPocArea", parea);
        }
        [HttpPost]
        public ActionResult NewPocArea(int parentid,FormCollection collection)
        {
            var depth = 1;
            var newParea = new PocArea();
            PocArea parent = null;
            try
            {
                if (parentid > 0)
                {
                    parent = _facilityService.GetPocAreaById(parentid);
                    depth = parent.Area.Depth + 1;
                    newParea.ParentId = parentid;
                }
                var area = _facilityService.GetAreaByDepth(depth);
                newParea.Area = area;
                newParea.AreaId = area.Id;
                UpdateModel(newParea);
                if (ValidateModel(newParea))
                {
                    _facilityService.CreatePocArea(newParea);
                    Messages.AddFlashMessageWithParams("PocAreaCreatedMessage", area.Name, newParea.Name);
                    if (parentid > 0)
                        return RedirectToAction("EditPocArea", "Area", new {id = parentid});

                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                Messages.AddException(ex);
            }

            ViewData["ParentPocArea"] = parent;
            return View("NewPocArea", newParea);
        }

        public ActionResult EditPocArea(int id)
        {
            var pocArea = _facilityService.GetPocAreaById(id);
            PocArea parent = null;
            if (pocArea.ParentId.HasValue)
            {
                parent = _facilityService.GetPocAreaById(pocArea.ParentId.Value);
            }
            var depth = _facilityService.GetDepthOfArea();
            if (depth > pocArea.Area.Depth)
            {
                ViewData["ChildAreaName"] = _facilityService.GetAreaByDepth(pocArea.Area.Depth + 1).Name;
            }
            else
            {
                ViewData["ChildAreaName"] = null;
            }

            ViewData["ParentPocArea"] = parent;
            ViewData["PocAreas"] = _facilityService.GetChildPocAreas(id);
            return View("EditPocArea", pocArea);
        }

        [HttpPost]
        public ActionResult EditPocArea(int id,FormCollection collection)
        {
            var depth = 1;
            var pocArea = _facilityService.GetPocAreaById(id);
            PocArea parent = null;
            try
            {
                if (pocArea.ParentId.HasValue)
                {
                    parent = _facilityService.GetPocAreaById(pocArea.ParentId.Value);
                }
                UpdateModel(pocArea);

                if (ValidateModel(pocArea))
                {
                    _facilityService.UpdatePocArea(pocArea);
                    Messages.AddFlashMessageWithParams("PocAreaEditedMessage", pocArea.Area.Name, pocArea.Name);

                    if (pocArea.ParentId.HasValue)
                        return RedirectToAction("EditPocArea", "Area", new {id = pocArea.ParentId.Value});

                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                Messages.AddException(ex);
            }

            depth = _facilityService.GetDepthOfArea();
            if (depth > pocArea.Area.Depth)
            {
                ViewData["ChildAreaName"] = _facilityService.GetAreaByDepth(pocArea.Area.Depth + 1).Name;
            }
            else
            {
                ViewData["ChildAreaName"] = null;
            }
            ViewData["ParentPocArea"] = parent;
            ViewData["PocAreas"] = _facilityService.GetChildPocAreas(id);

            return View("EditPocArea", pocArea);
        }
        public ActionResult DeletePocArea(int id)
        {
            var area = _facilityService.GetPocAreaById(id);
            try
            {
                _facilityService.DeletePocArea(area);

                Messages.AddFlashMessageWithParams("PocAreaDeletedMessage",area.Area.Name, area.Name);
            }
            catch (Exception ex)
            {
                Messages.AddFlashException(ex);
            }

            if (area.ParentId.HasValue)
                return RedirectToAction("EditPocArea", "Area", new { id = area.ParentId.Value });

            return RedirectToAction("Index");
        }

       
    }
}
