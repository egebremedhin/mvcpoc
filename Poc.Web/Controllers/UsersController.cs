﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Castle.Core.Internal;
using Poc.Core.Entities.AdminModule;
using Poc.Core.Enumerations;
using Poc.Service.AdminModule;
using Poc.Service.ModelValidators;
using Poc.Web.Helpers;
using Poc.Web.Models;
using Poc.Web.Mvc.Paging;
using Resources.Poc.Web;

namespace Poc.Web.Controllers
{
    public class UsersController : ManagerController
	{
		private readonly IUserService _userService;
        private IFacilityService _facilityService;
		private readonly RoleModelValidator _roleModelValidator;
		private const int PageSize = 20;

		public UsersController(IUserService userService,IFacilityService facilityService, UserModelValidator userModelValidator, RoleModelValidator roleModelValidator)
		{
			_userService = userService;
		    _facilityService = facilityService;
			ModelValidator = userModelValidator;
			_roleModelValidator = roleModelValidator;
		}

		public ActionResult Index(int? page)
		{
			return Browse(null, null, null, page);
		}

		public ActionResult Browse(string username, int? roleId, bool? isActive,int? page)
		{
			ViewData["username"] = username;
			ViewData["roles"] = new SelectList(_userService.GetAllRoles(), "RoleId", "Name", roleId);
			ViewData["roleid"] = roleId;
			IDictionary<bool, string> isActiveOptions = new Dictionary<bool, string>() { { true, GetText("Yes") }, { false, GetText("No") } };
			ViewData["isactiveoptions"] = new SelectList(isActiveOptions, "Key", "Value", isActive);
			ViewData["isactive"] = isActive;
			
			int totalCount;
			IList<AppUser> users = _userService.FindUsers(username, roleId, isActive,PageSize, page, out totalCount);
			return View("Index", new PagedList<AppUser>(users, page.HasValue ? page.Value -1 : 0, PageSize, totalCount));
		}

        public ActionResult New()
        {
            var areaAccessRightId = (Int32)AreaAccessRight.Global;
		    ViewData["Roles"] = _userService.GetAllRoles();
            ViewData["AreaAccessRights"] = new SelectList(ClearAreaAccessRights(EnumHelper.GetValuesWithDescription<AreaAccessRight>()), "Key", "Value", areaAccessRightId);
            ViewData["AreaAccessRightId"] = areaAccessRightId;
            
			var user = new AppUser();
            PopAreaRightAccess(areaAccessRightId, null, null, null);
			return View("NewUser", user);
		}

        public ActionResult Edit(int id)
		{
            var user = _userService.GetUserById(id);
		    ViewData["Roles"] = _userService.GetAllRoles();
            ViewData["AreaAccessRights"] = new SelectList(ClearAreaAccessRights(EnumHelper.GetValuesWithDescription<AreaAccessRight>()), "Key", "Value", user.AreaAccessRightId);
            ViewData["AreaAccessRightId"] = user.AreaAccessRightId;
            PopAreaRightAccess(user.AreaAccessRightId, user.LevelOnePocAreaId, user.LevelTwoPocAreaId, user.LevelThreePocAreaId);
			return View("EditUser", user);
		}

		[AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create(int[] roleIds, int? areaAccessRightId, int[] pocAreaIds, int? levelOnePocAreaId, int? levelTwoPocAreaId, int? levelThreePocAreaId, int[] facilityIds)
		{
			var newUser = new AppUser();
            var roles = _userService.GetAllRoles();
			try
			{
                UpdateModel(newUser, new[] { "UserName", "FirstName", "LastName", "Email", "IsActive" });
				newUser.Password = AppUser.HashPassword(Request.Form["Password"]);
				newUser.PasswordConfirmation = AppUser.HashPassword(Request.Form["PasswordConfirmation"]);
			    newUser.AreaAccessRightId = areaAccessRightId;
                newUser.AllowedPocAreas = GetAllowedPocAreas(areaAccessRightId, pocAreaIds, levelOnePocAreaId,
                    levelTwoPocAreaId, levelThreePocAreaId);
                newUser.AllowedFacilitys = GetAllowedFacilities(areaAccessRightId, facilityIds);
                UpdateUserRoles(newUser,roles, roleIds);

				if (ValidateModel(newUser))
				{
					_userService.CreateUser(newUser,roleIds);
					Messages.AddFlashMessageWithParams("UserCreatedMessage", newUser.UserName);
					return RedirectToAction("Index");
				}
			}
			catch (Exception ex)
			{
				Messages.AddException(ex);
			}
		    ViewData["Roles"] = roles;
            ViewData["AreaAccessRights"] = new SelectList(ClearAreaAccessRights(EnumHelper.GetValuesWithDescription<AreaAccessRight>()), "Key", "Value", areaAccessRightId);
            ViewData["areaAccessRightId"] = areaAccessRightId;
            PopAreaRightAccess(areaAccessRightId, levelOnePocAreaId, levelTwoPocAreaId, levelThreePocAreaId);
			return View("NewUser", newUser);
		}

		[AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update(int id, int[] roleIds, int? areaAccessRightId, int[] pocAreaIds, int? levelOnePocAreaId, int? levelTwoPocAreaId, int? levelThreePocAreaId, int[] facilityIds)
		{
			var user = _userService.GetUserById(id);
            var roles = _userService.GetAllRoles();
            try
            {
                UpdateModel(user, new[] { "UserName", "FirstName", "LastName", "Email", "IsActive" });
                user.AreaAccessRightId = areaAccessRightId;
                user.AllowedPocAreas = GetAllowedPocAreas(areaAccessRightId, pocAreaIds, levelOnePocAreaId,
                    levelTwoPocAreaId, levelThreePocAreaId);
                user.AllowedFacilitys = GetAllowedFacilities(areaAccessRightId, facilityIds);
                UpdateUserRoles(user, roles, roleIds);
                if (ValidateModel(user, new[] { "FirstName", "LastName", "Email", "Roles" }))
                {

                    _userService.UpdateUser(user, roleIds);
                    Messages.AddFlashMessageWithParams("UserUpdatedMessage", user.UserName);
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                Messages.AddException(ex);
            }
 
			ViewData["Roles"] = roles;
            ViewData["AreaAccessRights"] = new SelectList(ClearAreaAccessRights(EnumHelper.GetValuesWithDescription<AreaAccessRight>()), "Key", "Value", areaAccessRightId);
            ViewData["areaAccessRightId"] = areaAccessRightId;
		    PopAreaRightAccess(areaAccessRightId, levelOnePocAreaId, levelTwoPocAreaId, levelThreePocAreaId);
			return View("EditUser", user);
		}

        private Dictionary<string, string> ClearAreaAccessRights(Dictionary<string, string> rights)
        {
            var depth = _facilityService.GetDepthOfArea();
            if (depth <= 1)
            {
                rights.Remove("2");
                rights.Remove("3");
            }
            else if (depth == 2)
            {
                rights.Remove("3");
            }
            return rights;
        }
        private void PopAreaRightAccess(int? areaAccessRightId, int? levelOnePocAreaId, int? levelTwoPocAreaId,
            int? levelThreePocAreaId)
        {
            var areaDepth = _facilityService.GetDepthOfArea();
            var pareas = _facilityService.GetRootPocArea();

            ViewData["AreaLevel1"] = _facilityService.GetAreaByDepth(1);
            ViewData["AreaLevel2"] = _facilityService.GetAreaByDepth(2);
            if (areaDepth == 3)
                ViewData["AreaLevel3"] = _facilityService.GetAreaByDepth(3);

            switch (areaAccessRightId)
            {
                case 1:
                    ViewData["PocAreas"] = pareas;
                    break;
                case 2:
                    if (!levelOnePocAreaId.HasValue && pareas.Count > 0)
                    {
                        levelOnePocAreaId = pareas[0].Id;
                    }
                    ViewData["LevelOnePocAreas"] = new SelectList(pareas, "Id", "Name", levelOnePocAreaId);
                    ViewData["PocAreas"] = _facilityService.GetChildPocAreas(levelOnePocAreaId);
                    break;
                case 3:
                    if (!levelOnePocAreaId.HasValue && pareas.Count > 0)
                    {
                        levelOnePocAreaId = pareas[0].Id;
                    }

                    var l2Areas = _facilityService.GetChildPocAreas(levelOnePocAreaId);
                    if (!levelTwoPocAreaId.HasValue && l2Areas.Count > 0)
                    {
                        levelTwoPocAreaId = l2Areas[0].Id;
                    }

                    ViewData["LevelOnePocAreas"] = new SelectList(pareas, "Id", "Name", levelOnePocAreaId);
                    ViewData["LevelTwoPocAreas"] = new SelectList(l2Areas, "Id", "Name", levelTwoPocAreaId);
                    ViewData["PocAreas"] = _facilityService.GetChildPocAreas(levelTwoPocAreaId);
                    break;
                case 4:
                    if (!levelOnePocAreaId.HasValue && pareas != null && pareas.Count > 0)
                    {
                        levelOnePocAreaId = pareas[0].Id;
                    }

                    var pl2Area = _facilityService.GetChildPocAreas(levelOnePocAreaId);
                    if (!levelTwoPocAreaId.HasValue && pl2Area.Count > 0)
                    {
                        levelTwoPocAreaId = pl2Area[0].Id;
                    }
                    var pl3Area = _facilityService.GetChildPocAreas(levelTwoPocAreaId);
                    if (!levelThreePocAreaId.HasValue && pl3Area.Count > 0)
                    {
                        levelThreePocAreaId = pl3Area[0].Id;
                    }
                    ViewData["LevelOnePocAreas"] = new SelectList(pareas, "Id", "Name", levelOnePocAreaId);
                    ViewData["LevelTwoPocAreas"] = new SelectList(pl2Area, "Id", "Name", levelTwoPocAreaId);
                    if (_facilityService.GetDepthOfArea() == 3)
                    {
                        ViewData["LevelThreePocAreas"] = new SelectList(pl3Area, "Id", "Name", levelThreePocAreaId);
                        ViewData["Facilitys"] = _facilityService.GetAllFacilities(levelThreePocAreaId);
                    }
                    else
                    {
                        ViewData["Facilitys"] = _facilityService.GetAllFacilities(levelTwoPocAreaId);
                    }
                    break;
            }

        }

        private string GetAllowedPocAreas(int? areaAccessRightId, int[] pocAreaIds, int? levelOnePocAreaId, int? levelTwoPocAreaId, int? levelThreePocAreaId)
        {
            string areaIds = "";
            
            switch (areaAccessRightId)
            {
                case 0:
                    areaIds = "0";
                    break;
                case 1:
                    areaIds = string.Join("|", pocAreaIds.Select(x => x.ToString()).ToArray());
                    break;
                case 2:
                    areaIds = levelOnePocAreaId + "-" +
                              string.Join("|", pocAreaIds.Select(x => x.ToString()).ToArray());
                    break;
                case 3:
                    areaIds = levelOnePocAreaId + "-" + levelTwoPocAreaId + "-" +
                              string.Join("|", pocAreaIds.Select(x => x.ToString()).ToArray());
                    break;
                case 4:
                    if (_facilityService.GetDepthOfArea() == 2)
                    {
                        areaIds = levelOnePocAreaId + "-" + levelTwoPocAreaId;
                    }
                    else if (_facilityService.GetDepthOfArea() == 3)
                    {
                        areaIds = levelOnePocAreaId + "-" + levelTwoPocAreaId + "-" +
                                  levelThreePocAreaId;
                    }
                    break;
            }
            return areaIds;
        }

        private string GetAllowedFacilities(int? areaAccessRightId, int[] facilityIds)
        {
            if (facilityIds == null)
                return "-1";
            string areaIds = "";

            switch (areaAccessRightId)
            {
                case 0:
                case 1:
                case 2:
                case 3:
                    areaIds = "0";
                    break;
                case 4:
                        areaIds = string.Join("|", facilityIds.Select(x => x.ToString()).ToArray());
                    break;
            }
            return areaIds;
        }

        [HttpGet]
        public ActionResult OnAccessRightFilter(int id, string value)
        {
            var rightid = int.Parse(value);
            var user = id<=0?new AppUser() : _userService.GetUserById(id);
            PopAreaRightAccess(rightid, user.LevelOnePocAreaId, user.LevelTwoPocAreaId,
                user.LevelThreePocAreaId);

            if (rightid == 1)
            {
                return PartialView("_LevelOne", user);
            }
            if (rightid == 2)
            {
                return PartialView("_LevelTwo", user);
            }
            if (rightid == 3)
            {
                return PartialView("_LevelThree", user);
            }
            if (rightid == 4)
            {
                return PartialView("_LevelFacility", user);
            }

            return PartialView("_empty");
        }

        [HttpGet]
        public ActionResult OnPocAreaFilter(int id, string value)
        {
            var parentid = int.Parse(value);
            var user = id <= 0 ? new AppUser() : _userService.GetUserById(id);
            ViewData["PocAreas"] = _facilityService.GetChildPocAreas(parentid);

            return PartialView("_PocAreaSelector", user);
        }

        [HttpGet]
        public ActionResult OnPocAreaTwoFilter(int id, string value)
        {
            var parentid = int.Parse(value);
            //var user = id <= 0 ? new AppUser() : _userService.GetUserById(id);
            var result = _facilityService.GetChildPocAreas(parentid);

            var model = new PareaDropdownModel
            {
                ModelId = "LevelTwoPocAreaId",
                DataFilterUrl = Url.Action("OnPocAreaFilter", "Users", new { id = id }),
                DataUpdateContainer = "#divPocAreas"
            };

            foreach (var p in result)
            {
                model.DropdownItems.Add(new DropdownItem() {ItemId = p.Id, ItemName = p.Name});
            }
            model.DropdownItems.Insert(0, new DropdownItem() { ItemId = -1, ItemName = "---Select--" });
            return PartialView("_PocAreaDroplist", model);
        }
        [HttpGet]
        public ActionResult OnFacilityAreaTwoFilter(int id, string value)
        {
            var parentid = int.Parse(value);
            var result = _facilityService.GetChildPocAreas(parentid);
            var model = new PareaDropdownModel
            {
                ModelId = "LevelTwoPocAreaId",
                DataFilterUrl = @Url.Action("OnPocAreaThreeFilter", "Users", new {id = id})
            };
            int depth = _facilityService.GetDepthOfArea();
            if (depth == 3)
            {
                model.DataUpdateContainer = "#divLevelThreeParea";
                model.DataUpdateId = "#divPocAreas";
            }
            else
            {
                model.DataFilterUrl = @Url.Action("OnFacilityFilter", "Users", new {id = id});
                model.DataUpdateContainer = "#divPocAreas";
            }
            

            foreach (var p in result)
            {
                model.DropdownItems.Add(new DropdownItem() { ItemId = p.Id, ItemName = p.Name });
            }
            model.DropdownItems.Insert(0, new DropdownItem() { ItemId = -1, ItemName = "---Select--" });
            return PartialView("_PocAreaDroplist", model);
        }
        [HttpGet]
        public ActionResult OnPocAreaThreeFilter(int id, string value)
        {
            var parentid = int.Parse(value);
            var result = _facilityService.GetChildPocAreas(parentid);

            var model = new PareaDropdownModel
            {
                ModelId = "LevelThreePocAreaId",
                DataFilterUrl = Url.Action("OnFacilityFilter", "Users", new { id = id }),
                DataUpdateContainer = "#divPocAreas"
            };

            foreach (var p in result)
            {
                model.DropdownItems.Add(new DropdownItem() { ItemId = p.Id, ItemName = p.Name });
            }
            model.DropdownItems.Insert(0, new DropdownItem() { ItemId = -1, ItemName = "---Select--" });
            return PartialView("_PocAreaDroplist", model);
        }
        [HttpGet]
        public ActionResult OnFacilityFilter(int id, string value)
        {
            var areaid = int.Parse(value);
            var user = id <= 0 ? new AppUser() : _userService.GetUserById(id);
            ViewData["Facilitys"] = _facilityService.GetAllFacilities(areaid);

            return PartialView("_FacilitySelector", user);
        }
        private void UpdateUserRoles(AppUser user,IList<Role> roles, int[] roleIds)
        {
            if (roleIds == null || roleIds.Length == 0)
            {
                user.Roles.Clear();
                return;
            }

            var selectedRole = new HashSet<int>(roleIds);
            var userRoles = new HashSet<int>(user.Roles.Select(x => x.RoleId));

            foreach (var role in roles)
            {
                if (selectedRole.Contains(role.RoleId))
                {
                    if (!userRoles.Contains(role.RoleId))
                    {
                        user.Roles.Add(role);
                    }
                }
                else
                {
                    if (userRoles.Contains(role.RoleId))
                        user.Roles.Remove(role);
                }
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
		public ActionResult ChangePassword(int id, string password, string passwordConfirmation)
		{
			AppUser user = _userService.GetUserById(id);
			try
			{
				user.Password = AppUser.HashPassword(password);
				user.PasswordConfirmation = AppUser.HashPassword(passwordConfirmation);

				if (ValidateModel(user, new[] { "Password", "PasswordConfirmation" }))
				{
					_userService.UpdateUser(user);
					Messages.AddMessage("PasswordChangedMessage");
				}
			}
			catch (Exception ex)
			{
				Messages.AddException(ex);
			}
			ViewData["Roles"] = _userService.GetAllRoles();
            ViewData["AreaAccessRights"] = new SelectList(EnumHelper.GetValuesWithDescription<AreaAccessRight>(), "Key", "Value", user.AreaAccessRightId);
            ViewData["areaAccessRightId"] = user.AreaAccessRightId;
           
			return View("EditUser", user);
		}

		[AcceptVerbs(HttpVerbs.Post)]
		public ActionResult Delete(int id)
		{
			AppUser user = _userService.GetUserById(id);
			try
			{
				_userService.DeleteUser(user);
				Messages.AddFlashMessageWithParams("UserDeletedMessage", user.UserName);
			}
			catch(Exception ex)
			{
				Messages.AddFlashException(ex);
			}
			return RedirectToAction("Index");
		}

		[AcceptVerbs(HttpVerbs.Post)]
		public ActionResult CheckUsernameAvailability(string userName)
		{
			bool userExists = false;
			if (! String.IsNullOrEmpty(userName) && userName.Length > 3)
			{
				AppUser user = _userService.GetUserByUserName(userName);
				userExists = user != null;
			}
			return Json(!userExists);
		}

		public ActionResult Roles()
		{
		    IList<Role> roles = _userService.GetAllRoles();
			return View("Roles", roles);
		}

		public ActionResult NewRole()
		{
			//ViewData["Rights"] = this._userService.GetAllRights();
			Role role = new Role();
			return View(role);
		}

		public ActionResult EditRole(int id)
		{
			//ViewData["Rights"] = this._userService.GetAllRights();
			Role role = _userService.GetRoleById(id);
			return View(role);
		}

		[AcceptVerbs(HttpVerbs.Post)]
		public ActionResult CreateRole(int[] rightIds)
		{
			Role newRole = new Role();
			try
			{
				UpdateModel(newRole, "role");
               
				if (ValidateModel(newRole, _roleModelValidator, new [] { "Name" }))
				{
					_userService.CreateRole(newRole);
					Messages.AddFlashMessageWithParams("RoleCreatedMessage", newRole.Name);
					return RedirectToAction("roles");
				}
			}
			catch (Exception ex)
			{
				Messages.AddException(ex);
			}
			ViewData["Title"] = GetText("NewRolePageTitle");
			
			return View("NewRole", newRole);
		}

		[AcceptVerbs(HttpVerbs.Post)]
		public ActionResult UpdateRole(int id, int[] rightIds)
		{
			Role role = _userService.GetRoleById(id);
			try
			{
				UpdateModel(role, "role");

				if (ValidateModel(role, _roleModelValidator, new[] { "Name" }))
				{
					_userService.UpdateRole(role);
					Messages.AddFlashMessageWithParams("RoleUpdatedMessage", role.Name);
					return RedirectToAction("Roles");
				}
			}
			catch (Exception ex)
			{
				Messages.AddException(ex);
			}
			ViewData["Title"] = GetText("EditRolePageTitle");
			return View("EditRole", role);
		}

		[AcceptVerbs(HttpVerbs.Post)]
		public ActionResult DeleteRole(int id)
		{
			Role role = _userService.GetRoleById(id);
			try
			{
				_userService.DeleteRole(role);
				Messages.AddFlashMessageWithParams("RoleDeletedMessage", role.Name);
			}
			catch (Exception ex)
			{
				Messages.AddFlashException(ex);
			}
			return RedirectToAction("Roles");
		}
	}
}
