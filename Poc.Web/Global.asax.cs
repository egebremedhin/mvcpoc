﻿using System;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Castle.Windsor;
using Poc.Core.Util;
using Poc.Web.Components;
using log4net;
using log4net.Config;


namespace Poc.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : HttpApplication, IContainerAccessor
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(MvcApplication));
        private static readonly string ERROR_PAGE_LOCATION = "~/Error.aspx";

        /// <summary>
        /// Obtain the container.
        /// </summary>
        public IWindsorContainer Container
        {
            get { return IoC.Container; }
        }


        protected void Application_Start(Object sender, EventArgs e)
        {
            // Initialize log4net 
            XmlConfigurator.ConfigureAndWatch(new FileInfo(Server.MapPath("~/") + "config/logging.config"));


            // Initialize container
            Bootstrapper.InitializeContainer();

            // View engine
            AreaRegistration.RegisterAllAreas();
            //ViewEngines.Engines.Clear();
            //ViewEngines.Engines.Add(new PocAreaViewEngine());

            // Routes
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            //RouteHelper.RouteDebugger.RewriteRoutesForTesting(RouteTable.Routes);

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            if (Context != null && Context.IsCustomErrorEnabled)
            {
                Server.Transfer(ERROR_PAGE_LOCATION, false);
            }
        }

        protected void Application_End(object sender, EventArgs e)
        {
            Container.Dispose();
        }

    }
}