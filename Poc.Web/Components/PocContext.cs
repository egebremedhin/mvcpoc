using System.Threading;
using System.Web;
using Poc.Core;
using Poc.Core.Entities.AdminModule;
using Poc.Core.Util;

namespace Poc.Web.Components
{
    /// <summary>
    /// Provides a Poc-specific context, comparable to the HttpContext.
    /// </summary>
    public class PocContext : IPocContext
    {
        private AppUser _currentUser;

        /// <summary>
        /// The Poc user for the current request.
        /// </summary>
        public AppUser CurrentUser
        {
            get { return this._currentUser; }
        }
        
        /// <summary>
        /// Gets the current IPoc context.
        /// <remarks>
        /// This property is just for convenience. Only use it from places where the context can't be injected via IoC.
        /// </remarks>
        /// </summary>
        public static IPocContext Current
        {
            get { return IoC.Resolve<IPocContext>(); }
        }

        /// <summary>
        /// Set the Poc user for the current context.
        /// </summary>
        /// <param name="user"></param>
        public void SetUser(AppUser user)
        {
            this._currentUser = user;
            HttpContext.Current.User = user;
            Thread.CurrentPrincipal = user;
        }
    }
}
