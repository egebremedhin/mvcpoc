﻿using System;
using System.Reflection;
using System.Web.Mvc;
using Castle.Core.Logging;
using Castle.Services.Logging.Log4netIntegration;
using Castle.Windsor;
using Castle.MicroKernel.Registration;
using log4net;

using Poc.Core;
using Poc.Core.Service;
using Poc.Core.Util;
using Poc.Core.Validation;
using Poc.DAL;
using Poc.DAL.Ambient;
using Poc.DAL.Contracts;
using Poc.Service;
using Poc.Web.Mvc.Localization;
using Poc.Web.Mvc.Validation;
using Poc.Web.Mvc.Windsor;

namespace Poc.Web.Components
{
	/// <summary>
	/// Responsible for initializing the Poc application.
	/// </summary>
	public static class Bootstrapper
	{
		private static readonly ILog log = LogManager.GetLogger(typeof(Bootstrapper));

		/// <summary>
		/// Initialize Container
		/// </summary>
		/// 
		public static void InitializeContainer()
		{
			try
			{
				// Initialize Windsor
				IWindsorContainer container = new PocContainer();

				// Inititialize the static Windsor helper class. 
				IoC.Initialize(container);

				// Add IPocContext to the container.
				container.Register(Component.For<IPocContext>()
					.ImplementedBy<PocContext>()
					.Named("poc.context")
					.LifeStyle.PerWebRequest
				);

				// Add IPocContextProvider to the container.
				container.Register(Component.For<IPocContextProvider>()
					.ImplementedBy<PocContextProvider>()
				);


				// Windsor controller builder
				ControllerBuilder.Current.SetControllerFactory(new WindsorControllerFactory(container));

				// Register ASP.NET MVC components
				RegisterMvcComponents(container);

				// Validators
				RegisterValidatorComponents(container);

				// Localizer
				container.Register(Component.For<ILocalizer>().ImplementedBy<GlobalResourceLocalizer>());
			}
			catch (Exception ex)
			{
				log.Error("Error initializing application.", ex);
				throw;
			}
		}
        
		private static void RegisterMvcComponents(IWindsorContainer container)
		{
             container.Register(
                 Component.For<IDbContextScopeFactory>().ImplementedBy<DbContextScopeFactory>().LifestylePerWebRequest(),
                 
                Component.For<IAmbientDbContextLocator>().ImplementedBy<AmbientDbContextLocator>().LifestylePerWebRequest(),
		        
                Classes.FromThisAssembly().BasedOn<IController>().LifestyleTransient(),
                
                Classes.FromAssembly(Assembly.GetAssembly(typeof (IBaseRepository)))
		            .BasedOn<IBaseRepository>().WithService.DefaultInterfaces()
		            .LifestyleTransient(),

		        //All services
		        Classes.FromAssembly(Assembly.GetAssembly(typeof (IService)))
		            .BasedOn<IService>().WithService.DefaultInterfaces()
		            .LifestyleTransient());
		}

		private static void RegisterValidatorComponents(IWindsorContainer container)
		{
		    container.Register(
		        Component.For<IBrowserValidatorProvider>().ImplementedBy<JQueryValidator>(),

		        Component.For<ILocalizedValidatorRegistry>().ImplementedBy<CachedLocalizedValidatorRegistry>(),

		        // Register specific generic castle model validator
		        Component.For(typeof (IModelValidator<>)).ImplementedBy(typeof (CastleModelValidator<>))
		            .LifestyleTransient(),

		        // Register all model validators.
		        Classes.FromAssembly(Assembly.GetAssembly(typeof (IService)))
		            .BasedOn<IModelValidator>()
		            .LifestyleTransient(),

		        Component.For<BrowserValidationEngine>(),

		        //container.Kernel.AddComponentInstance("validationresources", Resources.Poc.ValidationMessages.ResourceManager);
		        Component.For(Resources.Poc.ValidationMessages.ResourceManager.GetType())
		            .Named("validationresources")
		            .Instance(Resources.Poc.ValidationMessages.ResourceManager));

			// Set validation engine to accessor.
			ValidationEngineAccessor.Current.SetValidationEngine(container.Resolve<BrowserValidationEngine>());
		}
	}
}
