using Castle.Windsor;
using Castle.Windsor.Configuration.Interpreters;

namespace Poc.Web.Components
{
	/// <summary>
	/// The PocContainer serves as the IoC container for Poc.
	/// </summary>
	public class PocContainer : WindsorContainer
	{
		/// <summary>
		/// Constructor. The configuration is read from the web.config.
		/// </summary>
		public PocContainer() : base(new XmlInterpreter())
		{
			RegisterFacilities();
			RegisterServices();
		}

		private void RegisterFacilities()
		{
		}

		private void RegisterServices()
		{
			
		}

	}
}
