﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Resources.Poc.Web;

namespace Poc.Web.Helpers
{
    public static class EnumHelper
    {
        public static SelectListItem[] GetListItems<T>(bool includeSelect)
        {
            IList<SelectListItem> items = GetValuesWithDescription<T>().Select(x => new SelectListItem { Text = x.Value, Value = x.Key }).ToList();
            if (includeSelect)
            {
                items.Insert(0, new SelectListItem { Value = String.Empty, Text = GlobalResources.SelectLabel });
            }
            return items.ToArray();
        }

        /// <summary>
        /// Gets a dictionary with description and values from the specified enumtype
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, string> GetValuesWithDescription<T>()
        {
            var descriptionandvalues = new Dictionary<string, string>();
            int i = 0;
            Type enumtype = typeof(T);
            foreach (string name in Enum.GetNames(enumtype))
            {
                Enum value = (Enum)Enum.GetValues(enumtype).GetValue(i);
                descriptionandvalues.Add(((int)Enum.GetValues(enumtype).GetValue(i)).ToString(), GetEnumDescription(value));
                i++;
            }
            return descriptionandvalues;
        }

        public static string GetEnumDescription<T>(T value)
        {
            ResourceManager resourceManager = Resources.Poc.Enums.ResourceManager;
            Type type = value.GetType();
            string resourceKey = type.Name + "_" + value;
            string description = resourceManager.GetString(resourceKey, Thread.CurrentThread.CurrentUICulture);
            if (string.IsNullOrEmpty(description))
            {
                description = value.ToString();
            }
            return description;
        }
    }
}