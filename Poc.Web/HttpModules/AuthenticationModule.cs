using System;
using System.Web;
using Poc.Core.Entities.AdminModule;
using Poc.Core.Util;
using Poc.Web.Components;
using Poc.Service.AdminModule;

namespace Poc.Web.HttpModules
{
	/// <summary>
	/// HttpModule to extend Forms Authentication.
	/// </summary>
	public class AuthenticationModule : IHttpModule
	{
		public void Init(HttpApplication context)
		{
			context.AuthenticateRequest += new EventHandler(Context_AuthenticateRequest);
		}

		public void Dispose()
		{
			// Nothing here	
		}

		private void Context_AuthenticateRequest(object sender, EventArgs e)
		{
			var app = (HttpApplication)sender;
			if (app.Context.User != null && app.Context.User.Identity.IsAuthenticated)
			{
				var userService = IoC.Resolve<IUserService>();

				// There is a logged-in user with a standard Forms Identity. Replace it with
				// poc identity (the User class implements IIdentity). 				
				int userId = Int32.Parse(app.Context.User.Identity.Name);
				AppUser pocUser = userService.GetSingle(userId);
				pocUser.IsAuthenticated = true;
				PocContext.Current.SetUser(pocUser);
			}
		}
	}
}
