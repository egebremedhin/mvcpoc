﻿/* Common javascript functionality for Poc manager, requires jQuery 1.2.x */

// jQuery extra's

// Delegate function for selector based event handling.
// Thanks to http://www.danwebb.net/2008/2/8/event-delegation-made-easy-in-jquery
jQuery.delegate = function(rules) {
    return function(e) {
        var target = $(e.target);
        for (var selector in rules)
            if (target.is(selector)) return rules[selector].apply(this, $.makeArray(arguments));
    }
};

// Global Poc manager js configuration properties
var PocConfig = function() {
    // Properties
    this.ContentDir = $Url.resolve('~/Content/');
    this.ConfirmText = 'Are you sure?';
};

$(document).ready(function() {

    // Add AJAX indicator
    $(document.body).ajaxStart(function() {
        $(document.body).append('<div id="loading"><img src="' + $Url.resolve('~/Content/Images/ajax-loader.gif') + '" alt="loading"/></div>');
        $('#loading').css({ position: "fixed", width: "40px", top: "50%", left: "50%" });
    }).ajaxStop(function() {
        $('#loading').remove();
    });

    $('#taskarea').click($.delegate({
        '.expandlink': function(e) {
            // Handle expand and collapse links
            $(e.target).next().show();
            $(e.target).removeClass('expandlink').addClass('collapselink');
            return false;
        },
        '.collapselink': function(e) {
            // Handle expand and collapse links
            $(e.target).next().hide();
            $(e.target).removeClass('collapselink').addClass('expandlink');
            return false;
        },
        '.deletelink': function(e) {
            // Attach a delete event handler to all a tags that have the deletelink class.
            submitAfterConfirm(e.target);
        }
    }));

    $('#contentarea').click($.delegate({
        '.deletelink, .abtndelete': function(e) {
            submitAfterConfirm(e.target);
        }
    }));

    // Wire up the click event of any current or future dialog links
    $('.dialogLink').live('click', function () {
        var element = $(this);

        // Retrieve values from the HTML5 data attributes of the link        
        var dialogTitle = element.attr('data-dialog-title');
        var updateTargetId = '#' + element.attr('data-update-target-id');
        var updateUrl = element.attr('data-update-url');

        // Generate a unique id for the dialog div
        var dialogId = 'uniqueName-' + Math.floor(Math.random() * 1000);
        var dialogDiv = "<div id='" + dialogId + "'></div>";

        // Load the form into the dialog div
        $(dialogDiv).load(this.href, function () {
            $(this).dialog({
                modal: true,
                resizable: false,
                title: dialogTitle,
                buttons: {
                    "Save": function () {
                        // Manually submit the form                        
                        var form = $('form', this);
                        $(form).submit();
                    },
                    "Cancel": function () { $(this).dialog('close'); }
                }
            });

            // Enable client side validation
            //$.validator.unobtrusive.parse(this);

            // Setup the ajax submit logic
            wireUpForm(this, updateTargetId, updateUrl);
        });
        return false;
    });

});

function displayMessages() {
	$("#messagewrapper").slideDown(500);
	
	$(document).click(function() { 
		$("#messagewrapper").empty(); 
	});
}

function processJsonMessage(messageData) {
	var messageTypes = ['Message','Error','Exception'];
	$.each(messageTypes, function(index, messageType) {
		var messageValue = eval('messageData.' + messageType);
		if (messageValue != "" && messageValue != undefined) {
		    $("#messagewrapper").append('<div class="' + messageType.toLowerCase() + 'box"><img src="' + $Url.resolve('~/Content/Images/cross.gif') + '" class="close_message" style="float:right;cursor:pointer" alt="Close" />' + messageValue + '</div>');
		}
	});
	displayMessages();
}

function movePartialMessages() {
	// When partial views are rendered, the PartialMessagesFilter might have added some messages to the partial view output. This method 
	// moves these messages to the main messages area.
	if ($('.partialmessagewrapper').children().length > 0 && $('#messagewrapper').length > 0) {
		$('#messagewrapper').empty();
		$('#messagewrapper').append($('.partialmessagewrapper').children());
		displayMessages();
	}
}

function submitAfterConfirm(targetElement) {
    if (confirm('Are you sure?')) {
		$(targetElement).parents('form').submit();
	}
}

function OnPocAreaDropFilter(me) {
    
    //var divid = 'divPocAreas';
    
    var value = me.options[me.selectedIndex].value; //(me.selectedIndex == '-1') ? '' : me.options[me.selectedIndex].value;
    var filterUrl = me.getAttribute('data-filter-url') + '?value=' + value;
    var updateContainer = me.getAttribute('data-update-container');
    var dataUpdateId = me.getAttribute('data-update-id');
    
    $.get(filterUrl, function(r) {
        $(updateContainer).html(r);
        
        if (dataUpdateId != null) {
            if (dataUpdateId.indexOf(',') > 0) {
                var sp = dataUpdateId.split(',');
                for (var i = 0; i < sp.length; i++)
                {
                    $(sp[i]).empty();
                }
            }
            else {
                $(dataUpdateId).empty();    
            }
        }
    });
}

function wireUpForm(dialog, updateTargetId, updateUrl) {
    $('form', dialog).submit(function () {

        // Do not submit if the form
        // does not pass client side validation
        if (!$(this).valid())
            return false;

        // Client side validation passed, submit the form
        // using the jQuery.ajax form
        $.ajax({
            url: this.action,
            type: this.method,
            data: $(this).serialize(),
            success: function (result)
            {
                // Check whether the post was successful
                if (result.success)
                {
                    // Close the dialog 
                    $(dialog).dialog('close');

                    // Reload the updated data in the target div
                    $(updateTargetId).load(updateUrl);
                }
                else
                {
                    // Reload the dialog to show model errors                    
                    $(dialog).html(result);

                    // Enable client side validation
                    //$.validator.unobtrusive.parse(dialog);

                    // Setup the ajax submit logic
                    wireUpForm(dialog, updateTargetId, updateUrl);
                }
            }
        });
        return false;
    });
}

