﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Poc.Web.Models
{
    public class DropdownItem
    {
        public int ItemId { get; set; }
        public string ItemName { get; set; }
    }

    public class PareaDropdownModel
    {
        public PareaDropdownModel()
        {
            DropdownItems = new List<DropdownItem>();
        }
        public string ModelId { get; set; }
        public List<DropdownItem> DropdownItems { get; set; }

        public string DataFilterUrl { get; set; }
        public string DataUpdateContainer { get; set; }
        public string DataUpdateId { get; set; }
    }


}