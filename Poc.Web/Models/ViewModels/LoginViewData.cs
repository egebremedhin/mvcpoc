﻿using Castle.Components.Validator;

namespace Poc.Web.Models.ViewModels
{
	public class LoginViewData
	{
		[ValidateNonEmpty("UserNameValidatorNonEmpty")]
		public string Username { get; set; }
		[ValidateNonEmpty("PasswordValidatorNonEmpty")]
		[ValidateLength(5, 50, "PasswordValidatorLength")]
		public string Password { get; set; }
	}
}