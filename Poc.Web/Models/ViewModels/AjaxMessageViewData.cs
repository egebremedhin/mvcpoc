﻿namespace Poc.Web.Models.ViewModels
{
	public class AjaxMessageViewData
	{
		public string Message 
		{ 
			get; set;
		}

		public string Error
		{
			get; set;
		}
	}
}
