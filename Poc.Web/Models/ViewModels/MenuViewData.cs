﻿using System.Collections.Generic;

namespace Poc.Web.Models.ViewModels
{
	public class MenuViewData
	{
		private readonly IList<MenuItemData> _standardMainMenuItems = new List<MenuItemData>();
		private readonly IList<MenuItemData> _optionalMainMenuItems = new List<MenuItemData>();

		public IList<MenuItemData> StandardMainMenuItems
		{
			get { return _standardMainMenuItems; }
		}

		public IList<MenuItemData> OptionalMainMenuItems
		{
			get { return _optionalMainMenuItems; }
		}

		public void AddStandardMenuItem(MenuItemData menuItemData)
		{
			_standardMainMenuItems.Add(menuItemData);
		}

		public void AddOptionalMenuItem(MenuItemData menuItemData)
		{
			_optionalMainMenuItems.Add(menuItemData);
		}
	}

	public class MenuItemData
	{
		private readonly string _url;
		private readonly string _text;
		private readonly bool _isSelected;
		private readonly string _iconUrl;
		private readonly IList<MenuItemData> _sideMenuItems;
	    private readonly IList<MenuItemData> _popupMenuItems; 
		public string Url
		{
			get { return _url; }
		}

		public string Text
		{
			get { return _text; }
		}

		public bool IsSelected
		{
			get { return _isSelected; }
		}

		public string IconUrl
		{
			get { return _iconUrl; }
		}

		public IList<MenuItemData> SideMenuItems
		{
			get { return _sideMenuItems; }
		}

        public IList<MenuItemData> PopUpMenuItems
        {
            get { return _popupMenuItems; }
        }
		public MenuItemData(string url, string text, bool isSelected, string iconUrl)
		{
			_sideMenuItems = new List<MenuItemData>();
            _popupMenuItems = new List<MenuItemData>();
			_url = url;
			_text = text;
			_isSelected = isSelected;
			_iconUrl = iconUrl;
		}

		public void AddSideMenuItem(MenuItemData childMenuItem)
		{
			_sideMenuItems.Add(childMenuItem);
		}
        public void AddPopUpMenuItem(MenuItemData childMenuItem)
        {
            _popupMenuItems.Add(childMenuItem);
        }
	}
}


