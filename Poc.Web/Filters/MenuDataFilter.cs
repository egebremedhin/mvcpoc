﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Poc.Core;
using Poc.Core.Entities.AdminModule;
using Poc.Core.Util;
using Poc.Service.AdminModule;
using Poc.Web.Models.ViewModels;

namespace Poc.Web.Filters
{
    public class MenuDataFilter : ActionFilterAttribute
    {
        private readonly IPocContext _pocContext;
        private readonly ITabService _tabService;
        private readonly IUserService _userService;
        private int _currentTabid;
        private int _currentPageid;
        /// <summary>
        /// Creates a new instance of the <see cref="MenuDataFilter"></see> class.
        /// </summary>
        /// <remarks>
        /// Lookup components via the static IoC resolver. It would be great if we could do this via dependency injection
        /// but filters cannot be managed via the container in the current version of ASP.NET MVC. 
        /// </remarks>
        public MenuDataFilter()
            : this(IoC.Resolve<IPocContext>(), IoC.Resolve<ITabService>(), IoC.Resolve<IUserService>())
        { }

        
        public MenuDataFilter(IPocContext pocContext, ITabService tabService,IUserService userService)
        {
            _pocContext = pocContext;
            _tabService = tabService;
            _userService = userService;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // Don't get menu data for ajax requests.
            if (!filterContext.HttpContext.Request.IsAjaxRequest())
            {
                InitMenuViewData(filterContext);
            }
        }

        private void InitMenuViewData(ActionExecutingContext filterContext)
        {
            var menuViewData = new MenuViewData();
            AppUser user = _pocContext.CurrentUser;

            if (filterContext.HttpContext.Request.Params["tabid"] != null)
                _currentTabid = Int32.Parse(filterContext.HttpContext.Request.Params["tabid"]);
            else
                _currentTabid = 0;
            
            if (filterContext.HttpContext.Request.Params["nodeid"] != null)
                _currentPageid = Int32.Parse(filterContext.HttpContext.Request.Params["nodeid"]);

            if (user != null && user.IsAuthenticated)
            {
                var urlHelper = new UrlHelper(filterContext.RequestContext);
                var roles = _userService.GetAllRoles();
                foreach (var id in _tabService.GetAllTabIds())
                {
                    var tab = _tabService.GetTabById(id);
                    if (tab.ViewAllowed(_pocContext.CurrentUser))
                    {
                        MenuItemData menuItemData = CreateMenuItemFromTab(tab, urlHelper);
                            if (tab.GetLeftSideTabNodes.Count > 0)
                            {
                                foreach (var childNode in tab.GetLeftSideTabNodes)
                                {
                                    if (childNode.IsActive && childNode.Node.HasPermission(user))
                                    {
                                        menuItemData.AddSideMenuItem(CreateMenuItemFromTabNode(childNode, urlHelper));
                                    }
                                }
                            }

                            if (tab.GetPopupTabNodes.Count > 0)
                            {
                                foreach (var childNode in tab.GetPopupTabNodes)
                                {
                                    if (childNode.IsActive && childNode.Node.HasPermission(user))
                                    {
                                        menuItemData.AddPopUpMenuItem(CreateMenuItemFromTabNode(childNode, urlHelper));
                                    }
                                }
                            }
                        menuViewData.AddStandardMenuItem(menuItemData);
                    }
                }
                if (user.HasAdminRight())
                    menuViewData.AddStandardMenuItem(CreateAdminMenuItem(urlHelper));
            }

            filterContext.Controller.ViewData.Add("MenuViewData", menuViewData);
        }

        private MenuItemData CreateMenuItemFromTab(Tab tab, UrlHelper urlHelper)
        {
            IDictionary<string, object> routeValues = new Dictionary<string, object>();

            if (string.IsNullOrEmpty(tab.ControllerName))
                routeValues.Add("controller", "Home");
            else
                routeValues.Add("controller", tab.ControllerName);

            routeValues.Add("action", "Index");
            routeValues.Add("tabid", tab.TabId);

            string url = "";
            var httpContext = new HttpContextWrapper(HttpContext.Current);
            RouteData routeData = RouteTable.Routes.GetRouteData(httpContext);
            if (routeData != null)
            {
                VirtualPathData virtualPath = routeData.Route.GetVirtualPath(
                    new RequestContext(httpContext, routeData), new RouteValueDictionary(routeValues));

                if (virtualPath != null)
                {
                    url = "~/" + virtualPath.VirtualPath;
                }
            }

            return new MenuItemData(VirtualPathUtility.ToAbsolute(url), tab.TabName, _currentTabid==tab.TabId,
                tab.Icon != null ? urlHelper.Content("~/Content/images/" + tab.Icon) : null);
        }

        private MenuItemData CreateMenuItemFromTabNode(Page page, UrlHelper urlHelper)
        {
            IDictionary<string, object> routeValues = new Dictionary<string, object>();

            routeValues.Add("controller", page.Node.ControllerName);
            routeValues.Add("action", page.Node.ActionName);
            routeValues.Add("tabid", page.TabId);
            routeValues.Add("nodeid", page.PageId);

            string url = "";
            var httpContext = new HttpContextWrapper(HttpContext.Current);
            RouteData routeData = RouteTable.Routes.GetRouteData(httpContext);
            if (routeData != null)
            {
                VirtualPathData virtualPath = routeData.Route.GetVirtualPath(
                    new RequestContext(httpContext, routeData), new RouteValueDictionary(routeValues));

                if (virtualPath != null)
                {
                    url = "~/" + virtualPath.VirtualPath;
                }
            }

            return new MenuItemData(VirtualPathUtility.ToAbsolute(url), page.Node.Name, _currentPageid == page.PageId, null);
        }

        private MenuItemData CreateAdminMenuItem(UrlHelper urlHelper)
        {
            IDictionary<string, object> routeValues = new Dictionary<string, object>();
            routeValues.Add("controller", "Home");
            routeValues.Add("action", "Index");
            routeValues.Add("tabid", 0);

            string url = "";
            var httpContext = new HttpContextWrapper(HttpContext.Current);
            RouteData routeData = RouteTable.Routes.GetRouteData(httpContext);
            if (routeData != null)
            {
                VirtualPathData virtualPath = routeData.Route.GetVirtualPath(
                    new RequestContext(httpContext, routeData), new RouteValueDictionary(routeValues));

                if (virtualPath != null)
                {
                    url = "~/" + virtualPath.VirtualPath;
                }
            }
           
            return new MenuItemData(VirtualPathUtility.ToAbsolute(url), "Admin", _currentTabid == 0, null);
        }

    }
}