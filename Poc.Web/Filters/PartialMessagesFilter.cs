﻿using System.Web.Mvc;

namespace Poc.Web.Filters
{
	/// <summary>
	/// While rendering partial views, we have no possiblity to show messages in the standard Poc way.
	/// This filter adds a little bit of javascript that displays the messages. So when you want to use the standard
	/// ShowMessage() etc stuff with partial views, decorate the action methods with this attribute.
	/// </summary>
	public class PartialMessagesFilter : ActionFilterAttribute
	{
		public override void  OnResultExecuting(ResultExecutingContext filterContext)
		{
			var result = new PartialViewResult();
			result.ViewName = "PartialMessages";
			result.ViewData.Model = filterContext.Controller.ViewData["Messages"];
			result.ExecuteResult(filterContext.Controller.ControllerContext);
 			base.OnResultExecuting(filterContext);
		}
	}
}
