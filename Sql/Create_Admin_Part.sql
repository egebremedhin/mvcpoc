SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AppUserRoles](
	[AppUserId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
 CONSTRAINT [PK_AppUserRoles] PRIMARY KEY CLUSTERED 
(
	[AppUserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AppUsers](
	[AreaAccessRightId] [int] NULL,
	[AppUserId] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](100) NOT NULL,
	[FirstName] [nvarchar](64) NOT NULL,
	[LastName] [nvarchar](64) NOT NULL,
	[Email] [nvarchar](100) NULL,
	[IsActive] [bit] NOT NULL,
	[LastLogin] [datetime] NULL,
	[LastIp] [nvarchar](40) NULL,
	[AllowedPocAreas] [nvarchar](400) NULL,
	[AllowedFacilitys] [nvarchar](400) NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateModified] [datetime] NULL,
 CONSTRAINT [PK_AppUser] PRIMARY KEY CLUSTERED 
(
	[AppUserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AreaAccessRights](
	[Id] [int] NOT NULL,
	[EnumName] [nvarchar](50) NOT NULL,
	[DisplayName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_AreaAccessRights] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Areas](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Depth] [int] NOT NULL,
 CONSTRAINT [PK_Areas] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Facilitys](
	[PocAreaId] [int] NOT NULL,
	[FacilityTypeId] [int] NOT NULL,
	[FacilityId] [int] IDENTITY(1,1) NOT NULL,
	[FacilityName] [nvarchar](96) NOT NULL,
	[FacilityLevel] [nvarchar](24) NULL,
	[FacilityCode] [nvarchar](24) NULL,
	[AreaId1] [int] NULL,
	[AreaId2] [int] NULL,
 CONSTRAINT [PK_Facilitys] PRIMARY KEY CLUSTERED 
(
	[FacilityId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NodeRoles](
	[NodeId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
 CONSTRAINT [PK_NodeRoles] PRIMARY KEY CLUSTERED 
(
	[NodeId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Nodes](
	[NodeId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](64) NOT NULL,
	[Description] [nvarchar](400) NULL,
	[ActionName] [nvarchar](100) NOT NULL,
	[ControllerName] [nvarchar](100) NOT NULL,
	[ShowInNavigation] [bit] NOT NULL,
 CONSTRAINT [PK_Nodes] PRIMARY KEY CLUSTERED 
(
	[NodeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pages](
	[TabId] [int] NOT NULL,
	[NodeId] [int] NOT NULL,
	[PageId] [int] IDENTITY(1,1) NOT NULL,
	[Position] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[MenuType] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Pages] PRIMARY KEY CLUSTERED 
(
	[PageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PocAreas](
	[ParentId] [int] NULL,
	[AreaId] [int] NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[ShortName] [nvarchar](16) NULL,
	[Depth] [int] NOT NULL,
	[Code] [nvarchar](32) NULL,
	[MapColor] [nvarchar](50) NULL,
	[MapId] [nvarchar](50) NULL,
	[SortOrder] [int] NULL,
	[AreaParentList] [nvarchar](400) NULL,
 CONSTRAINT [PK_District] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[RoleId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](32) NOT NULL,
	[Description] [nvarchar](128) NULL,
	[IsSysAdmin] [bit] NOT NULL,
 CONSTRAINT [PK_roles] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TabRoles](
	[TabId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
 CONSTRAINT [PK_TabRoles] PRIMARY KEY CLUSTERED 
(
	[TabId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tabs](
	[TabId] [int] IDENTITY(1,1) NOT NULL,
	[TabName] [nvarchar](50) NOT NULL,
	[Position] [int] NOT NULL,
	[ControllerName] [nvarchar](100) NOT NULL,
	[Icon] [nvarchar](50) NULL,
 CONSTRAINT [PK_Tabs] PRIMARY KEY CLUSTERED 
(
	[TabId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[AppUserRoles] ([AppUserId], [RoleId]) VALUES (1, 1)
INSERT [dbo].[AppUserRoles] ([AppUserId], [RoleId]) VALUES (1, 2)
INSERT [dbo].[AppUserRoles] ([AppUserId], [RoleId]) VALUES (2, 2)
INSERT [dbo].[AppUserRoles] ([AppUserId], [RoleId]) VALUES (5, 2)
SET IDENTITY_INSERT [dbo].[AppUsers] ON 

INSERT [dbo].[AppUsers] ([AreaAccessRightId], [AppUserId], [UserName], [Password], [FirstName], [LastName], [Email], [IsActive], [LastLogin], [LastIp], [AllowedPocAreas], [AllowedFacilitys], [DateCreated], [DateModified]) VALUES (0, 1, N'admin', N'5f4dcc3b5aa765d61d8327deb882cf99', N'Elias', N'Gebremedhin', N'elias@inbox.com', 1, CAST(0x0000A4A2016468B3 AS DateTime), N'::1', NULL, NULL, CAST(0x0000A48A01526100 AS DateTime), NULL)
INSERT [dbo].[AppUsers] ([AreaAccessRightId], [AppUserId], [UserName], [Password], [FirstName], [LastName], [Email], [IsActive], [LastLogin], [LastIp], [AllowedPocAreas], [AllowedFacilitys], [DateCreated], [DateModified]) VALUES (0, 2, N'marta', N'5f4dcc3b5aa765d61d8327deb882cf99', N'Marta', N'Asnakew', N'marta@inbox.com', 1, CAST(0x0000A4990032BE84 AS DateTime), N'::1', NULL, NULL, CAST(0x0000A499000FA354 AS DateTime), CAST(0x0000A49900325C75 AS DateTime))
INSERT [dbo].[AppUsers] ([AreaAccessRightId], [AppUserId], [UserName], [Password], [FirstName], [LastName], [Email], [IsActive], [LastLogin], [LastIp], [AllowedPocAreas], [AllowedFacilitys], [DateCreated], [DateModified]) VALUES (3, 5, N'hewan', N'81dc9bdb52d04dc20036dbd8313ed055', N'Hewan', N'Elias', N'hewan@inbox.com', 1, CAST(0x0000A49D01466A6C AS DateTime), N'::1', N'4-5-6', N'-1', CAST(0x0000A499003288FD AS DateTime), CAST(0x0000A4A20147EA3D AS DateTime))
SET IDENTITY_INSERT [dbo].[AppUsers] OFF
INSERT [dbo].[AreaAccessRights] ([Id], [EnumName], [DisplayName]) VALUES (0, N'Global', N'Global')
INSERT [dbo].[AreaAccessRights] ([Id], [EnumName], [DisplayName]) VALUES (1, N'AreaAccessLevel1', N'Area Access Level 1')
INSERT [dbo].[AreaAccessRights] ([Id], [EnumName], [DisplayName]) VALUES (2, N'AreaAccessLevel2', N'Area Access Level 2')
INSERT [dbo].[AreaAccessRights] ([Id], [EnumName], [DisplayName]) VALUES (3, N'AreaAccessLevel3', N'Area Access Level 3')
SET IDENTITY_INSERT [dbo].[Areas] ON 

INSERT [dbo].[Areas] ([Id], [Name], [Depth]) VALUES (1, N'Region', 1)
INSERT [dbo].[Areas] ([Id], [Name], [Depth]) VALUES (2, N'Zone', 2)
INSERT [dbo].[Areas] ([Id], [Name], [Depth]) VALUES (3, N'Woreda', 3)
SET IDENTITY_INSERT [dbo].[Areas] OFF
INSERT [dbo].[NodeRoles] ([NodeId], [RoleId]) VALUES (5, 1)
INSERT [dbo].[NodeRoles] ([NodeId], [RoleId]) VALUES (5, 2)
INSERT [dbo].[NodeRoles] ([NodeId], [RoleId]) VALUES (6, 1)
INSERT [dbo].[NodeRoles] ([NodeId], [RoleId]) VALUES (6, 2)
INSERT [dbo].[NodeRoles] ([NodeId], [RoleId]) VALUES (12, 1)
SET IDENTITY_INSERT [dbo].[Nodes] ON 

INSERT [dbo].[Nodes] ([NodeId], [Name], [Description], [ActionName], [ControllerName], [ShowInNavigation]) VALUES (5, N'Dashboard', NULL, N'Index', N'Dashboard', 1)
INSERT [dbo].[Nodes] ([NodeId], [Name], [Description], [ActionName], [ControllerName], [ShowInNavigation]) VALUES (6, N'Home', NULL, N'Index', N'Home', 1)
INSERT [dbo].[Nodes] ([NodeId], [Name], [Description], [ActionName], [ControllerName], [ShowInNavigation]) VALUES (7, N'Home-About', NULL, N'About', N'Home', 1)
INSERT [dbo].[Nodes] ([NodeId], [Name], [Description], [ActionName], [ControllerName], [ShowInNavigation]) VALUES (8, N'Home-Contact', NULL, N'Contact', N'Home', 1)
INSERT [dbo].[Nodes] ([NodeId], [Name], [Description], [ActionName], [ControllerName], [ShowInNavigation]) VALUES (9, N'Login-Index', NULL, N'Index', N'Login', 1)
INSERT [dbo].[Nodes] ([NodeId], [Name], [Description], [ActionName], [ControllerName], [ShowInNavigation]) VALUES (10, N'Login-Login', NULL, N'Login', N'Login', 1)
INSERT [dbo].[Nodes] ([NodeId], [Name], [Description], [ActionName], [ControllerName], [ShowInNavigation]) VALUES (11, N'Login-Logout', NULL, N'Logout', N'Login', 1)
INSERT [dbo].[Nodes] ([NodeId], [Name], [Description], [ActionName], [ControllerName], [ShowInNavigation]) VALUES (12, N'Nodes-Index', NULL, N'Index', N'Nodes', 1)
INSERT [dbo].[Nodes] ([NodeId], [Name], [Description], [ActionName], [ControllerName], [ShowInNavigation]) VALUES (13, N'Nodes-Create', NULL, N'Create', N'Nodes', 1)
INSERT [dbo].[Nodes] ([NodeId], [Name], [Description], [ActionName], [ControllerName], [ShowInNavigation]) VALUES (14, N'Nodes-Edit', NULL, N'Edit', N'Nodes', 1)
INSERT [dbo].[Nodes] ([NodeId], [Name], [Description], [ActionName], [ControllerName], [ShowInNavigation]) VALUES (15, N'Nodes-Delete', NULL, N'Delete', N'Nodes', 1)
INSERT [dbo].[Nodes] ([NodeId], [Name], [Description], [ActionName], [ControllerName], [ShowInNavigation]) VALUES (16, N'Nodes-PermissionsImport', NULL, N'PermissionsImport', N'Nodes', 1)
INSERT [dbo].[Nodes] ([NodeId], [Name], [Description], [ActionName], [ControllerName], [ShowInNavigation]) VALUES (17, N'Tabs-Index', NULL, N'Index', N'Tabs', 1)
INSERT [dbo].[Nodes] ([NodeId], [Name], [Description], [ActionName], [ControllerName], [ShowInNavigation]) VALUES (18, N'Tabs-Details', NULL, N'Details', N'Tabs', 1)
INSERT [dbo].[Nodes] ([NodeId], [Name], [Description], [ActionName], [ControllerName], [ShowInNavigation]) VALUES (19, N'Tabs-Create', NULL, N'Create', N'Tabs', 1)
INSERT [dbo].[Nodes] ([NodeId], [Name], [Description], [ActionName], [ControllerName], [ShowInNavigation]) VALUES (20, N'Tabs-Edit', NULL, N'Edit', N'Tabs', 1)
INSERT [dbo].[Nodes] ([NodeId], [Name], [Description], [ActionName], [ControllerName], [ShowInNavigation]) VALUES (21, N'Tabs-Delete', NULL, N'Delete', N'Tabs', 1)
INSERT [dbo].[Nodes] ([NodeId], [Name], [Description], [ActionName], [ControllerName], [ShowInNavigation]) VALUES (22, N'Users-Index', NULL, N'Index', N'Users', 1)
INSERT [dbo].[Nodes] ([NodeId], [Name], [Description], [ActionName], [ControllerName], [ShowInNavigation]) VALUES (23, N'Users-Browse', NULL, N'Browse', N'Users', 1)
INSERT [dbo].[Nodes] ([NodeId], [Name], [Description], [ActionName], [ControllerName], [ShowInNavigation]) VALUES (24, N'Users-New', NULL, N'New', N'Users', 1)
INSERT [dbo].[Nodes] ([NodeId], [Name], [Description], [ActionName], [ControllerName], [ShowInNavigation]) VALUES (25, N'Users-Edit', NULL, N'Edit', N'Users', 1)
INSERT [dbo].[Nodes] ([NodeId], [Name], [Description], [ActionName], [ControllerName], [ShowInNavigation]) VALUES (26, N'Users-Create', NULL, N'Create', N'Users', 1)
INSERT [dbo].[Nodes] ([NodeId], [Name], [Description], [ActionName], [ControllerName], [ShowInNavigation]) VALUES (27, N'Users-Update', NULL, N'Update', N'Users', 1)
INSERT [dbo].[Nodes] ([NodeId], [Name], [Description], [ActionName], [ControllerName], [ShowInNavigation]) VALUES (28, N'Users-ChangePassword', NULL, N'ChangePassword', N'Users', 1)
INSERT [dbo].[Nodes] ([NodeId], [Name], [Description], [ActionName], [ControllerName], [ShowInNavigation]) VALUES (29, N'Users-Delete', NULL, N'Delete', N'Users', 1)
INSERT [dbo].[Nodes] ([NodeId], [Name], [Description], [ActionName], [ControllerName], [ShowInNavigation]) VALUES (30, N'Users-CheckUsernameAvailability', NULL, N'CheckUsernameAvailability', N'Users', 1)
INSERT [dbo].[Nodes] ([NodeId], [Name], [Description], [ActionName], [ControllerName], [ShowInNavigation]) VALUES (31, N'Users-Roles', NULL, N'Roles', N'Users', 1)
INSERT [dbo].[Nodes] ([NodeId], [Name], [Description], [ActionName], [ControllerName], [ShowInNavigation]) VALUES (32, N'Users-NewRole', NULL, N'NewRole', N'Users', 1)
INSERT [dbo].[Nodes] ([NodeId], [Name], [Description], [ActionName], [ControllerName], [ShowInNavigation]) VALUES (33, N'Users-EditRole', NULL, N'EditRole', N'Users', 1)
INSERT [dbo].[Nodes] ([NodeId], [Name], [Description], [ActionName], [ControllerName], [ShowInNavigation]) VALUES (34, N'Users-CreateRole', NULL, N'CreateRole', N'Users', 1)
INSERT [dbo].[Nodes] ([NodeId], [Name], [Description], [ActionName], [ControllerName], [ShowInNavigation]) VALUES (35, N'Users-UpdateRole', NULL, N'UpdateRole', N'Users', 1)
INSERT [dbo].[Nodes] ([NodeId], [Name], [Description], [ActionName], [ControllerName], [ShowInNavigation]) VALUES (36, N'Users-DeleteRole', NULL, N'DeleteRole', N'Users', 1)
SET IDENTITY_INSERT [dbo].[Nodes] OFF
SET IDENTITY_INSERT [dbo].[Pages] ON 

INSERT [dbo].[Pages] ([TabId], [NodeId], [PageId], [Position], [IsActive], [MenuType]) VALUES (1, 12, 1, 1, 1, N'LeftSide')
INSERT [dbo].[Pages] ([TabId], [NodeId], [PageId], [Position], [IsActive], [MenuType]) VALUES (1, 17, 2, 0, 1, N'LeftSide')
INSERT [dbo].[Pages] ([TabId], [NodeId], [PageId], [Position], [IsActive], [MenuType]) VALUES (1, 23, 3, 2, 1, N'LeftSide')
INSERT [dbo].[Pages] ([TabId], [NodeId], [PageId], [Position], [IsActive], [MenuType]) VALUES (2, 5, 4, 0, 1, N'LeftSide')
INSERT [dbo].[Pages] ([TabId], [NodeId], [PageId], [Position], [IsActive], [MenuType]) VALUES (2, 6, 5, 1, 1, N'LeftSide')
INSERT [dbo].[Pages] ([TabId], [NodeId], [PageId], [Position], [IsActive], [MenuType]) VALUES (2, 12, 6, 2, 1, N'LeftSide')
SET IDENTITY_INSERT [dbo].[Pages] OFF
SET IDENTITY_INSERT [dbo].[PocAreas] ON 

INSERT [dbo].[PocAreas] ([ParentId], [AreaId], [Id], [Name], [ShortName], [Depth], [Code], [MapColor], [MapId], [SortOrder], [AreaParentList]) VALUES (NULL, 1, 1, N'Addis ababa', N'AA', 0, N'AA', NULL, NULL, 1, NULL)
INSERT [dbo].[PocAreas] ([ParentId], [AreaId], [Id], [Name], [ShortName], [Depth], [Code], [MapColor], [MapId], [SortOrder], [AreaParentList]) VALUES (1, 2, 2, N'Estern', NULL, 1, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PocAreas] ([ParentId], [AreaId], [Id], [Name], [ShortName], [Depth], [Code], [MapColor], [MapId], [SortOrder], [AreaParentList]) VALUES (2, 3, 3, N'Woreda 4', NULL, 2, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PocAreas] ([ParentId], [AreaId], [Id], [Name], [ShortName], [Depth], [Code], [MapColor], [MapId], [SortOrder], [AreaParentList]) VALUES (NULL, 1, 4, N'Amahara', NULL, 0, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PocAreas] ([ParentId], [AreaId], [Id], [Name], [ShortName], [Depth], [Code], [MapColor], [MapId], [SortOrder], [AreaParentList]) VALUES (4, 2, 5, N'Gonder', NULL, 1, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PocAreas] ([ParentId], [AreaId], [Id], [Name], [ShortName], [Depth], [Code], [MapColor], [MapId], [SortOrder], [AreaParentList]) VALUES (5, 3, 6, N'Kebele2', NULL, 2, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[PocAreas] ([ParentId], [AreaId], [Id], [Name], [ShortName], [Depth], [Code], [MapColor], [MapId], [SortOrder], [AreaParentList]) VALUES (4, 2, 7, N'keble4', NULL, 3, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[PocAreas] OFF
SET IDENTITY_INSERT [dbo].[Roles] ON 

INSERT [dbo].[Roles] ([RoleId], [Name], [Description], [IsSysAdmin]) VALUES (1, N'Administrator', N'Administrator', 1)
INSERT [dbo].[Roles] ([RoleId], [Name], [Description], [IsSysAdmin]) VALUES (2, N'Authenticated user', N'Authenticated user', 0)
SET IDENTITY_INSERT [dbo].[Roles] OFF
INSERT [dbo].[TabRoles] ([TabId], [RoleId]) VALUES (1, 1)
INSERT [dbo].[TabRoles] ([TabId], [RoleId]) VALUES (2, 1)
INSERT [dbo].[TabRoles] ([TabId], [RoleId]) VALUES (3, 1)
INSERT [dbo].[TabRoles] ([TabId], [RoleId]) VALUES (4, 1)
INSERT [dbo].[TabRoles] ([TabId], [RoleId]) VALUES (5, 1)
INSERT [dbo].[TabRoles] ([TabId], [RoleId]) VALUES (5, 2)
SET IDENTITY_INSERT [dbo].[Tabs] ON 

INSERT [dbo].[Tabs] ([TabId], [TabName], [Position], [ControllerName], [Icon]) VALUES (1, N'User Profile', 2, N'Users', NULL)
INSERT [dbo].[Tabs] ([TabId], [TabName], [Position], [ControllerName], [Icon]) VALUES (2, N'Inventories', -1, N'Inventory', NULL)
INSERT [dbo].[Tabs] ([TabId], [TabName], [Position], [ControllerName], [Icon]) VALUES (3, N'Admin', -1, N'Tabs', NULL)
INSERT [dbo].[Tabs] ([TabId], [TabName], [Position], [ControllerName], [Icon]) VALUES (4, N'Inventories', -1, N'Inventory', NULL)
INSERT [dbo].[Tabs] ([TabId], [TabName], [Position], [ControllerName], [Icon]) VALUES (5, N'Inventories', -1, N'Inventory', NULL)
SET IDENTITY_INSERT [dbo].[Tabs] OFF
ALTER TABLE [dbo].[AppUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AppUserRoles_AppUsers] FOREIGN KEY([AppUserId])
REFERENCES [dbo].[AppUsers] ([AppUserId])
GO
ALTER TABLE [dbo].[AppUserRoles] CHECK CONSTRAINT [FK_AppUserRoles_AppUsers]
GO
ALTER TABLE [dbo].[AppUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AppUserRoles_Roles] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Roles] ([RoleId])
GO
ALTER TABLE [dbo].[AppUserRoles] CHECK CONSTRAINT [FK_AppUserRoles_Roles]
GO
ALTER TABLE [dbo].[AppUsers]  WITH CHECK ADD  CONSTRAINT [FK_AppUser_AreaAccessRights] FOREIGN KEY([AreaAccessRightId])
REFERENCES [dbo].[AreaAccessRights] ([Id])
GO
ALTER TABLE [dbo].[AppUsers] CHECK CONSTRAINT [FK_AppUser_AreaAccessRights]
GO
ALTER TABLE [dbo].[Facilitys]  WITH CHECK ADD  CONSTRAINT [FK_Facilitys_PocAreas] FOREIGN KEY([PocAreaId])
REFERENCES [dbo].[PocAreas] ([Id])
GO
ALTER TABLE [dbo].[Facilitys] CHECK CONSTRAINT [FK_Facilitys_PocAreas]
GO
ALTER TABLE [dbo].[NodeRoles]  WITH CHECK ADD  CONSTRAINT [FK_NodeRoles_Nodes] FOREIGN KEY([NodeId])
REFERENCES [dbo].[Nodes] ([NodeId])
GO
ALTER TABLE [dbo].[NodeRoles] CHECK CONSTRAINT [FK_NodeRoles_Nodes]
GO
ALTER TABLE [dbo].[NodeRoles]  WITH CHECK ADD  CONSTRAINT [FK_NodeRoles_Roles] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Roles] ([RoleId])
GO
ALTER TABLE [dbo].[NodeRoles] CHECK CONSTRAINT [FK_NodeRoles_Roles]
GO
ALTER TABLE [dbo].[Pages]  WITH CHECK ADD  CONSTRAINT [FK_Pages_Nodes] FOREIGN KEY([NodeId])
REFERENCES [dbo].[Nodes] ([NodeId])
GO
ALTER TABLE [dbo].[Pages] CHECK CONSTRAINT [FK_Pages_Nodes]
GO
ALTER TABLE [dbo].[Pages]  WITH CHECK ADD  CONSTRAINT [FK_Pages_Tabs] FOREIGN KEY([TabId])
REFERENCES [dbo].[Tabs] ([TabId])
GO
ALTER TABLE [dbo].[Pages] CHECK CONSTRAINT [FK_Pages_Tabs]
GO
ALTER TABLE [dbo].[PocAreas]  WITH CHECK ADD  CONSTRAINT [FK_PocAreas_Areas] FOREIGN KEY([AreaId])
REFERENCES [dbo].[Areas] ([Id])
GO
ALTER TABLE [dbo].[PocAreas] CHECK CONSTRAINT [FK_PocAreas_Areas]
GO
ALTER TABLE [dbo].[PocAreas]  WITH CHECK ADD  CONSTRAINT [FK_PocAreas_PocAreas1] FOREIGN KEY([ParentId])
REFERENCES [dbo].[PocAreas] ([Id])
GO
ALTER TABLE [dbo].[PocAreas] CHECK CONSTRAINT [FK_PocAreas_PocAreas1]
GO
ALTER TABLE [dbo].[TabRoles]  WITH CHECK ADD  CONSTRAINT [FK_TabRoles_Roles] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Roles] ([RoleId])
GO
ALTER TABLE [dbo].[TabRoles] CHECK CONSTRAINT [FK_TabRoles_Roles]
GO
ALTER TABLE [dbo].[TabRoles]  WITH CHECK ADD  CONSTRAINT [FK_TabRoles_Tabs] FOREIGN KEY([TabId])
REFERENCES [dbo].[Tabs] ([TabId])
GO
ALTER TABLE [dbo].[TabRoles] CHECK CONSTRAINT [FK_TabRoles_Tabs]
GO
