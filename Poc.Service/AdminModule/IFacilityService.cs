﻿
using System.Collections.Generic;
using Poc.Core.Entities.AdminModule;

namespace Poc.Service.AdminModule
{
    public interface IFacilityService : IService
    {
        Facility GetFacilityById(int facilityId);
        void CreateFacility(Facility facility);
        void UpdateFacility(Facility facility);
        void DeleteFacility(Facility facility);
        IList<Facility> GetAllFacilities(int? pocAreaId);

        Area GetAreaById(int id);
        int GetDepthOfArea();
        Area GetAreaByDepth(int depth);
        void CreateArea(Area area);
        void UpdateArea(Area area);
        void DeleteArea(Area area);
        IList<Area> GetAllAreas();

        PocArea GetPocAreaById(int id);
        void CreatePocArea(PocArea pocarea);
        void UpdatePocArea(PocArea pocarea);
        void DeletePocArea(PocArea pocarea);
        IList<PocArea> GetPocAreasByAreaId(int areaid);
        IList<PocArea> GetChildPocAreas(int? parentid);
        IList<PocArea> GetRootPocArea();
        
        
    }
}
