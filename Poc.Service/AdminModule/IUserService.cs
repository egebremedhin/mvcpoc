﻿using System.Collections.Generic;
using Poc.Core.Entities.AdminModule;

namespace Poc.Service.AdminModule
{
    public interface IUserService : IService
    {
        /// <summary>
        /// Get user by id
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        AppUser GetUserById(int userId);

        /// <summary>
        /// Get user by id with roles
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        AppUser GetSingle(int userId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <param name="hashedPassword"></param>
        /// <returns></returns>
        AppUser GetUserByUsernameAndPassword(string username, string hashedPassword);

        /// <summary>
        /// Get a user by username.
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        AppUser GetUserByUserName(string userName);

        /// <summary>
        /// /
        /// </summary>
        /// <param name="username"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        AppUser GetUserByUsernameAndEmail(string username, string email);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="searchString"></param>
        /// <returns></returns>
        IList<AppUser> FindUsersByUsername(string searchString);

        /// <summary>
        /// Create new user
        /// </summary>
        /// <param name="user"></param>
        void CreateUser(AppUser user, int[] roleIds);

        /// <summary>
        /// Update an existing user.
        /// </summary>
        /// <param name="user"></param>
        void UpdateUser(AppUser user, int[] roleIds);

        void UpdateUser(AppUser user);
        /// <summary>
        /// Delete an existing user.
        /// </summary>
        /// <param name="user"></param>
        void DeleteUser(AppUser user);

        /// <summary>
        /// Reset the password of the user with the given username and email address.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="email"></param>
        /// <returns>The new password</returns>
        string ResetPassword(string username, string email);

        /// <summary>
        /// Get all available roles.
        /// </summary>
        /// <returns></returns>
        IList<Role> GetAllRoles();

        /// <summary>
        /// Get a single role by id.
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        Role GetRoleById(int roleId);

        /// <summary>
        /// Get roles for the given id's.
        /// </summary>
        /// <param name="roleIds"></param>
        /// <returns></returns>
        IList<Role> GetRolesByIds(int[] roleIds);

        /// <summary>
        /// Create a new role.
        /// </summary>
        /// <param name="role"></param>
        /// <param name="currentSite"></param>
        void CreateRole(Role role);

        /// <summary>
        /// Update a role.
        /// </summary>
        /// <param name="role"></param>
        /// <param name="site"></param>
        void UpdateRole(Role role);

        /// <summary>
        /// Delete a role.
        /// </summary>
        /// <param name="role"></param>
        void DeleteRole(Role role);

        Role GetRoleByName(string name);

        IList<AppUser> FindUsers(string username, int? roleId, bool? isActive, int pageSize, int? page,
            out int totalCount);
    }
}
