
using System.Collections.Generic;
using Poc.Core.Entities.AdminModule;

namespace Poc.Service.AdminModule
{
    public interface INodeService : IService
    {
        /// <summary>
        /// Get a node by id.
        /// </summary>
        /// <param name="nodeId"></param>
        /// <returns></returns>
        Node GetNodeById(int nodeId);

        /// <summary>
        /// get node by controller name and action name
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        Node GetNode(string controller, string action);
        /// <summary>
        /// Save a node to the database.
        /// </summary>
        /// <param name="node"></param>
        void SaveNode(Node node, int[] viewRoleIds);

        /// <summary>
        /// Update an exisiting node.
        /// </summary>
        /// <param name="node"></param>
        void UpdateNode(Node node, int[] viewRoleIds);

        /// <summary>
        /// Delete an exising node.
        /// </summary>
        /// <param name="node"></param>
        void DeleteNode(Node node);
        
        /// <summary>
        /// Set node permissions.
        /// </summary>
        /// <param name="node">The node to set the permissions for.</param>
        /// <param name="viewRoleIds">Array of role id's that are allowed to view the node.</param>
        void SetNodePermissions(Node node, int[] viewRoleIds);

        IList<Node> GetAllNodes();
    }
}
