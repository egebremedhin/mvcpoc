﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Poc.Core.Entities.AdminModule;
using Poc.Core.Enumerations;
using Poc.DAL;
using Poc.DAL.Ambient;
using Poc.DAL.Contracts.AdminModule;

namespace Poc.Service.AdminModule
{
    public class TabService : ITabService
    {
        private readonly ITabRepository _tabRepository;
        private readonly IUserService _userService;
        private readonly IDbContextScopeFactory _dbContextScopeFactory;

        public TabService(IDbContextScopeFactory dbContextScopeFactory, ITabRepository tabRepository, IUserService userService)
        {
            _tabRepository = tabRepository;
            _userService = userService;
            _dbContextScopeFactory = dbContextScopeFactory;
        }
     
        public IList<Tab> GetAllTabs()
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                return _tabRepository.Query(null, x => x.Roles, x => x.Pages.Select(y => y.Node.Roles)).ToList();
            }
        }

        public Tab GetTabById(int id)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
               return _tabRepository.Get(x => x.TabId == id, x => x.Roles, x => x.Pages.Select(y => y.Node.Roles));
            }
        }

        public List<int> GetAllTabIds()
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                return _tabRepository.All().Select(x => x.TabId).ToList();
            }
        }
        public void CreateTab(Tab tab, int[] roleIds)
        {
            using (var dbContextScope = _dbContextScopeFactory.Create())
            {
                UpdateTabRoles(tab, _userService.GetAllRoles(), roleIds);
                _tabRepository.Add(tab);
                dbContextScope.SaveChanges();
            }
        }

        public void UpdateTab(Tab tab,int[] roleIds)
        {
            using (var dbContextScope = _dbContextScopeFactory.Create())
            {
                var orTab = _tabRepository.Get(x => x.TabId == tab.TabId, x => x.Roles);
                orTab.TabName = tab.TabName;
                orTab.ControllerName = tab.ControllerName;
                UpdateTabRoles(orTab, _userService.GetAllRoles(), roleIds);
                
                _tabRepository.Update(orTab);
                dbContextScope.SaveChanges();
            }
        }
        private void UpdateTabRoles(Tab tab, IList<Role> roles, int[] roleIds)
        {
            if (roleIds == null || roleIds.Length == 0)
            {
                tab.Roles.Clear();
                return;
            }

            var selectedRole = new HashSet<int>(roleIds);
            var tabRoles = new HashSet<int>(tab.Roles.Select(x => x.RoleId));

            foreach (var role in roles)
            {
                if (selectedRole.Contains(role.RoleId))
                {
                    if (!tabRoles.Contains(role.RoleId))
                    {
                        tab.Roles.Add(role);
                    }
                }
                else
                {
                    if (tabRoles.Contains(role.RoleId))
                    {
                        tab.Roles.Remove(role);
                    }
                }
            }
        }
        public void UpdateTab(int tabid,IList<Page> pages )
        {
            using (var dbContextScope = _dbContextScopeFactory.Create())
            {
                var t = _tabRepository.Get(tabid);
                foreach (var p in pages)
                {
                    t.Pages.Add(p);
                }
                _tabRepository.Update(t);
                dbContextScope.SaveChanges();
            }
        }
      
        public void MovePage(int tabid, int pageid, NodePositionMovement direction)
        {
            using (var dbContextScope = _dbContextScopeFactory.Create())
            {
                var tab = _tabRepository.Get(x => x.TabId == tabid, x => x.Pages);
                var tn = tab.Pages.Single(x => x.PageId == pageid);
                tn.Move(tab.Pages.OrderBy(x=>x.Position).ToList(), direction);
                _tabRepository.Update(tab);
                dbContextScope.SaveChanges();
            }
        }

        public void DeletePage(int id, int pageid)
        {
            using (var dbContextScope = _dbContextScopeFactory.Create())
            {
                var tab = _tabRepository.Get(x => x.TabId == id, x => x.Pages);
                var tn = tab.Pages.Single(x => x.PageId == pageid);
                tab.Pages.Remove(tn);

                int pos = 0;
                foreach (var p in tab.Pages.OrderBy(x=>x.Position))
                {
                    p.Position = pos;
                    pos++;
                }
                _tabRepository.Update(tab);
                dbContextScope.SaveChanges();
            }
            
        }

        public int GetPageCount(int id)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                var tab =_tabRepository.Get(x => x.TabId == id, x => x.Pages);
                return tab.Pages.Count;
            }
        }
    }
}
