﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using Poc.Core;
using Poc.Core.Entities.AdminModule;
using Poc.Core.Util;
using Poc.DAL;
using Poc.DAL.Ambient;
using Poc.DAL.Contracts;
using Poc.DAL.Contracts.AdminModule;

namespace Poc.Service.AdminModule
{
     public class UserService : IUserService
     {
         private readonly IUserRepository _userRepository;
         private readonly IRoleRepository _roleRepository;
         private readonly IDbContextScopeFactory _dbContextScopeFactory;
         private readonly ICommonRepository _commonRepository;
         public UserService(IDbContextScopeFactory dbContextScopeFactory, IUserRepository userRepository, IRoleRepository roleRepository,ICommonRepository commonRepository)
         {
             if (dbContextScopeFactory == null) 
                 throw new ArgumentNullException("dbContextScopeFactory");
             _dbContextScopeFactory = dbContextScopeFactory;
             _userRepository = userRepository;
             _roleRepository = roleRepository;
             _commonRepository = commonRepository;
         }

        public AppUser GetUserById(int userId)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                return _userRepository.Get(x => x.AppUserId == userId, x => x.Roles);
            }
        }

        public AppUser GetSingle(int userId)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                return _userRepository.Get(x => x.AppUserId == userId, x => x.Roles);
            }
        }
         public AppUser GetUserByUsernameAndPassword(string username , string hashedPassword )
         {
             using (_dbContextScopeFactory.CreateReadOnly())
             {
                 return _userRepository.Get(x => x.UserName == username && x.Password == hashedPassword,x=>x.Roles);
             }
         }
        public AppUser GetUserByUserName(string userName)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                return
                    _userRepository.Get(x => x.UserName.Equals(userName, StringComparison.CurrentCultureIgnoreCase));
            }
        }

        public AppUser GetUserByUsernameAndEmail(string username, string email)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                return _userRepository.Get(x => x.UserName == username && x.Email == email);
            }
        }

        public IList<AppUser> FindUsersByUsername(string searchString)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                return _userRepository.Query(x => x.UserName.StartsWith(searchString)).ToList();
            }
        }

        public void CreateUser(AppUser user,int[] roleIds)
        {
            using (var dbContextScope = _dbContextScopeFactory.Create())
            {
                var newUser = new AppUser
                {
                    UserName = user.UserName,
                    Password = user.Password,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    AreaAccessRightId = user.AreaAccessRightId,
                    Email = user.Email,
                    IsActive = user.IsActive,
                    AllowedPocAreas = user.AllowedPocAreas,
                    AllowedFacilitys = user.AllowedFacilitys,
                    DateModified = DateTime.Now,
                    DateCreated = DateTime.Now
                };

                var roles = _roleRepository.All().ToList();

                UpdateUserRoles(newUser, roles, roleIds);
                _userRepository.Add(newUser);
                dbContextScope.SaveChanges();
            }
        }

        public void UpdateUser(AppUser user)
        {
            using (var dbContextScope = _dbContextScopeFactory.Create())
            {
                _userRepository.Update(user);
                dbContextScope.SaveChanges();
            }
        }
        public void UpdateUser(AppUser user, int[] roleIds)
        {
            using (var dbContextScope = _dbContextScopeFactory.Create())
            {
                var originalUser = _userRepository.Get(x => x.AppUserId == user.AppUserId, x => x.Roles);
                originalUser.FirstName = user.FirstName;
                originalUser.LastName = user.LastName;
                originalUser.AreaAccessRightId = user.AreaAccessRightId;
                originalUser.Email = user.Email;
                originalUser.IsActive = user.IsActive;
                originalUser.AllowedPocAreas = user.AllowedPocAreas;
                originalUser.AllowedFacilitys = user.AllowedFacilitys;
                originalUser.DateModified = DateTime.Now;
                
                var roles = _roleRepository.All().ToList();
                UpdateUserRoles(originalUser, roles, roleIds);
                _userRepository.Update(originalUser);
                dbContextScope.SaveChanges();
            }
        }
        private void UpdateUserRoles(AppUser user, IList<Role> roles, int[] roleIds)
        {
            if (roleIds == null || roleIds.Length == 0)
            {
                user.Roles.Clear();
                return;
            }

            var selectedRole = new HashSet<int>(roleIds);
            var userRoles = new HashSet<int>(user.Roles.Select(x => x.RoleId));

            foreach (var role in roles)
            {
                if (selectedRole.Contains(role.RoleId))
                {
                    if (!userRoles.Contains(role.RoleId))
                    {
                        user.Roles.Add(role);
                        //role.AppUsers.Add(user);
                    }
                }
                else
                {
                    if (userRoles.Contains(role.RoleId))
                    {
                        user.Roles.Remove(role);
                        //role.AppUsers.Remove(user);
                    }
                }
            }
        }
        public void DeleteUser(AppUser user)
        {
            var currentUser = Thread.CurrentPrincipal as AppUser;
            if (currentUser != null && currentUser.AppUserId == user.AppUserId)
            {
                throw new DeleteForbiddenException("DeleteYourselfNotAllowedException");
            }
            using (var dbContextScope = _dbContextScopeFactory.Create())
            {
                _userRepository.Delete(user);
                dbContextScope.SaveChanges();
            }
        }

        public string ResetPassword(string username, string email)
        {
            using (var dbContextScope = _dbContextScopeFactory.Create())
            {
                AppUser user = _userRepository.Get(x => x.UserName == username && x.Email == email);
                if (user == null)
                {
                    throw new NullReferenceException("No user found with the given username and email");
                }
                string newPassword = user.GeneratePassword();
                _userRepository.Update(user);
                dbContextScope.SaveChanges();
                return newPassword;
            }
        }

        public IList<Role> GetAllRoles()
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                return _roleRepository.All().ToList();
            }
        }

        public Role GetRoleById(int roleId)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                return _roleRepository.Get(x => x.RoleId == roleId);
            }
        }

        public IList<Role> GetRolesByIds(int[] roleIds)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                return _roleRepository.All(x => roleIds.Contains(x.RoleId)).ToList();
            }
        }

        public void CreateRole(Role role)
        {
            using (var dbContextScope = _dbContextScopeFactory.Create())
            {
                _roleRepository.Add(role);
                dbContextScope.SaveChanges();
            }
        }

        public void UpdateRole(Role role)
        {
            using (var dbContextScope = _dbContextScopeFactory.Create())
            {
                _roleRepository.Update(role);
                dbContextScope.SaveChanges();
            }
        }

        public void DeleteRole(Role role)
        {
            using (var dbContextScope = _dbContextScopeFactory.Create())
            {
                _roleRepository.Delete(role);
                dbContextScope.SaveChanges();
            }
        }


         public IList<AppUser> FindUsers(string username, int? roleId, bool? isActive, int pageSize, int? page,
             out int totalCount)
         {
             if (!page.HasValue)
             {
                 page = 1;
             }
             using (_dbContextScopeFactory.CreateReadOnly())
             {
                 return _userRepository.FindUsers(username, roleId, isActive, pageSize, page.Value, out totalCount);
             }
         }
         public Role GetRoleByName(string name)
         {
             using (_dbContextScopeFactory.CreateReadOnly())
             {
                 return _roleRepository.Get(x => x.Name == name);
             }
         }

     }
}
