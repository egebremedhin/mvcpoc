﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Poc.Core.Entities.AdminModule;
using Poc.DAL.Ambient;
using Poc.DAL.Contracts;
using Poc.DAL.Contracts.AdminModule;

namespace Poc.Service.AdminModule
{
    public class FacilityService : IFacilityService
    {
        private readonly IFacilityRepository _facilityRepository;
        private readonly IDbContextScopeFactory _dbContextScopeFactory;
        private readonly ICommonRepository _commonRepository;
         public FacilityService(IDbContextScopeFactory dbContextScopeFactory,IFacilityRepository facilityRepository,ICommonRepository commonRepository)
         {
             if (dbContextScopeFactory == null) 
                 throw new ArgumentNullException("dbContextScopeFactory");
             _dbContextScopeFactory = dbContextScopeFactory;
             _facilityRepository = facilityRepository;
             _commonRepository = commonRepository;
         }
        public Facility GetFacilityById(int facilityId)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                return _facilityRepository.Get(x => x.FacilityId == facilityId, x => x.PocArea);
            }
        }

        public void CreateFacility(Facility facility)
        {
            using (var dbcontext = _dbContextScopeFactory.Create())
            {
               _facilityRepository.Add(facility);
                dbcontext.SaveChanges();
            }
        }

        public void UpdateFacility(Facility facility)
        {
            using (var dbcontext = _dbContextScopeFactory.Create())
            {
                //var ofacility = _facilityRepository.Get(facility.FacilityId);
                _facilityRepository.Update(facility);
                dbcontext.SaveChanges();
            }
        }

        public void DeleteFacility(Facility facility)
        {
            using (var dbcontext = _dbContextScopeFactory.Create())
            {
                _facilityRepository.Delete(facility);
                dbcontext.SaveChanges();
            }
        }

        public IList<Facility> GetAllFacilities(int? pocAreaId)
        {
            if(pocAreaId ==null)
                return new List<Facility>();
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                return _facilityRepository.All(x => x.PocArea.Id == pocAreaId).ToList();
            }
        }

        public Area GetAreaById(int id)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                return _commonRepository.Single<Area>(x => x.Id == id);
            }
        }

        public Area GetAreaByDepth(int depth)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                return _commonRepository.Single<Area>(x => x.Depth == depth);
            }
        }

        public int GetDepthOfArea()
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                return _commonRepository.Count<Area>();
            }
        }
        public void CreateArea(Area area)
        {
            using (var dbcontext = _dbContextScopeFactory.Create())
            {
                _commonRepository.Add(area);
                dbcontext.SaveChanges();
            }
        }

        public void UpdateArea(Area area)
        {
            using (var dbcontext = _dbContextScopeFactory.Create())
            {
                _commonRepository.Update(area);
                dbcontext.SaveChanges();
            }
        }

        public void DeleteArea(Area area)
        {
            using (var dbcontext = _dbContextScopeFactory.Create())
            {
                _commonRepository.Delete(area);
                dbcontext.SaveChanges();
            }
        }

        public IList<Area> GetAllAreas()
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
               return _commonRepository.All<Area>().ToList();
            }
        }

        public PocArea GetPocAreaById(int id)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                return _commonRepository.Single<PocArea>(x => x.Id == id, x => x.Area);
            }
        }

        public void CreatePocArea(PocArea pocarea)
        {
            using (var dbcontext = _dbContextScopeFactory.Create())
            {
                pocarea.Area = _commonRepository.Single<Area>(x => x.Id == pocarea.AreaId);
                _commonRepository.Add(pocarea);
                dbcontext.SaveChanges();
            }
        }

        public void UpdatePocArea(PocArea pocarea)
        {
            using (var dbcontext = _dbContextScopeFactory.Create())
            {
                var pa = _commonRepository.Single<PocArea>(x => x.Id == pocarea.Id, x => x.Area);
                pa.Code = pocarea.Code;
                pa.MapColor = pocarea.MapColor;
                pa.MapId = pocarea.MapId;
                pa.Name = pocarea.Name;
                pa.ShortName = pocarea.ShortName;
                
                _commonRepository.Update(pa);
                dbcontext.SaveChanges();
            }
        }

        public void DeletePocArea(PocArea pocarea)
        {
            using (var dbcontext = _dbContextScopeFactory.Create())
            {
                _commonRepository.Delete(pocarea);
                dbcontext.SaveChanges();
            }
        }

        public IList<PocArea> GetPocAreasByAreaId(int areaid)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                return _commonRepository.Query<PocArea>(x => x.AreaId == areaid, x => x.Area).ToList();
            }
        }

        public IList<PocArea> GetChildPocAreas(int? parentid)
        {
            if(parentid == null)
                return new List<PocArea>();
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                return _commonRepository.Query<PocArea>(x => x.ParentId == parentid, x => x.Area).ToList();
            }
        }

        public IList<PocArea> GetRootPocArea()
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                return _commonRepository.Query<PocArea>(x => x.ParentId == null, x => x.Area).ToList();
            }
        }
    }
}
