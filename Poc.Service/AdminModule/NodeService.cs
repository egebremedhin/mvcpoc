﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Poc.Core.Entities.AdminModule;
using Poc.DAL.Ambient;
using Poc.DAL.Contracts.AdminModule;

namespace Poc.Service.AdminModule
{
    public class NodeService : INodeService
    {
        private readonly INodeRepository _nodeRepository;
        private readonly IRoleRepository _roleRepository;
        private readonly IDbContextScopeFactory _dbContextScopeFactory;

         public NodeService(IDbContextScopeFactory dbContextScopeFactory, INodeRepository nodeRepository, IRoleRepository roleRepository)
         {
             if (dbContextScopeFactory == null) 
                 throw new ArgumentNullException("dbContextScopeFactory");
             _dbContextScopeFactory = dbContextScopeFactory;
             _roleRepository = roleRepository;
             _nodeRepository = nodeRepository;
         }
        public Node GetNodeById(int nodeId)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                return _nodeRepository.Get(x => x.NodeId == nodeId, x => x.Roles);
            }
        }
        public Node GetNode(string controller, string action)
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                return _nodeRepository.Get(x => x.ControllerName.Equals(controller) && x.ActionName.Equals(action));
            }
        }
        public void SaveNode(Node node,int[] viewRoleIds)
        {
            using (var dbContext =_dbContextScopeFactory.Create())
            {
                SetNodePermissions(node, viewRoleIds);
                _nodeRepository.Add(node);
                dbContext.SaveChanges();
            }
        }

        public void UpdateNode(Node node, int[] viewRoleIds)
        {
            using (var dbContext = _dbContextScopeFactory.Create())
            {
                var originalNode = _nodeRepository.Get(node.NodeId);
                originalNode.Name = node.Name;
                originalNode.Description = node.Description;
                originalNode.ControllerName = node.ControllerName;
                originalNode.ActionName = node.ActionName;
                originalNode.ShowInNavigation = node.ShowInNavigation;
                
                var roles = _roleRepository.All().ToList();
                UpdateNodeRoles(originalNode, roles, viewRoleIds);
                _nodeRepository.Update(originalNode);
                dbContext.SaveChanges();
            }
        }

        public void DeleteNode(Node node)
        {
            using (var dbContext = _dbContextScopeFactory.Create())
            {
                _nodeRepository.Delete(node);
                dbContext.SaveChanges();
            }
        }

        public void SetNodePermissions(Node node, int[] viewRoleIds)
        {
            int len = viewRoleIds != null ? viewRoleIds.Length : 0;
            using (_dbContextScopeFactory.Create())
            {
                for (int i = 0; i < len; i++)
                {
                    var role = _roleRepository.Get(viewRoleIds[i]);
                    node.Roles.Add(role);
                }
            }
        }
        private void UpdateNodeRoles(Node node, IList<Role> roles, int[] roleIds)
        {
            if (roleIds == null || roleIds.Length == 0)
            {
                node.Roles.Clear(); 
                return;
            }

            var selectedRole = new HashSet<int>(roleIds);
            var nodeRoles = new HashSet<int>(node.Roles.Select(x => x.RoleId));

            foreach (var role in roles)
            {
                if (selectedRole.Contains(role.RoleId))
                {
                    if (!nodeRoles.Contains(role.RoleId))
                    {
                        node.Roles.Add(role);
                    }
                }
                else
                {
                    if (nodeRoles.Contains(role.RoleId))
                        node.Roles.Remove(role);
                }
            }
        }
        public IList<Node> GetAllNodes()
        {
            using (_dbContextScopeFactory.CreateReadOnly())
            {
                return _nodeRepository.All(x=>x.Roles).ToList();
            }
        }
    }
}
