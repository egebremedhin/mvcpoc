﻿
using System.Collections.Generic;
using System.Reflection.Emit;
using System.Security.Cryptography.X509Certificates;
using Poc.Core.Entities.AdminModule;
using Poc.Core.Enumerations;

namespace Poc.Service.AdminModule
{
    public interface ITabService : IService
    {
        Tab GetTabById(int id);
        IList<Tab> GetAllTabs();
        void CreateTab(Tab tab, int[] roleIds);
        void UpdateTab(Tab tab, int[] roleIds);
        void UpdateTab(int tabid, IList<Page> pages);
        void MovePage(int tabid, int pageid, NodePositionMovement direction);
        void DeletePage(int id, int pageid);
        int GetPageCount(int tabid);
        List<int> GetAllTabIds();
    }
}
