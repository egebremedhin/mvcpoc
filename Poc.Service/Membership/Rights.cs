namespace Poc.Service.Membership
{
	/// <summary>
	/// Core Rights constants.
	/// </summary>
	public static class Rights
	{
		public const string Authenticated = "Authenticated";
		public const string Editor = "Editor";
		public const string Administrator = "Administrator";

		public const string ManageUsers  = "Manage Users";
		public const string ManageServer = "Manage Server";
		public const string AccessAdmin  = "Access Admin";
		
	}
}
