using System;
using System.Security.Authentication;
using Poc.DAL.Contracts.AdminModule;
using Poc.Core.Entities.AdminModule;
using Poc.Service.AdminModule;

namespace Poc.Service.Membership
{
	public class DefaultAuthenticationService : IAuthenticationService
	{
		private readonly IUserService _userService;

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="userService"></param>
		public DefaultAuthenticationService(IUserService userService)
		{
			_userService = userService;
		}

		#region IAuthenticationService Members

		public AppUser AuthenticateUser(string username, string password, string ipAddress)
		{
			if (String.IsNullOrEmpty(username) || String.IsNullOrEmpty(password))
			{
				throw new AuthenticationException("EmptyUsernameOrPassword");
			}

            try
            {
                string hashedPassword = AppUser.HashPassword(password);
                var user = _userService.GetUserByUsernameAndPassword(username, hashedPassword);
                if (user == null)
                {
                    throw new AuthenticationException("InvalidUsernamePassword");
                }
                user.LastIp = ipAddress;
                user.LastLogin = DateTime.Now;
                _userService.UpdateUser(user);
                user.IsAuthenticated = true;
                return user;
            }
            catch (Exception ex)
            {
                throw new AuthenticationException("AuthenticationException", ex);
            }
		}

		#endregion
	}
}
