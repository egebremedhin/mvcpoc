﻿using System;
using System.Collections.Generic;
using Poc.Core.Entities.AdminModule;
using Poc.Core.Validation;
using Poc.Service.AdminModule;

namespace Poc.Service.ModelValidators
{
	public class UserModelValidator : CastleModelValidator<AppUser>
	{
		private readonly IUserService _userService;

		public UserModelValidator(IUserService userService)
		{
			_userService = userService;
		}

		protected override void PerformValidation(AppUser objectToValidate, ICollection<string> includeProperties)
		{
			base.PerformValidation(objectToValidate, includeProperties);
            
		    if (ShouldValidateProperty("Roles", includeProperties) && objectToValidate.Roles.Count <= 0)
		    {
                AddError("Roles", "RolesValidatorNotEmpty", true);
		    }
			// Check username uniqueness.
			if (ShouldValidateProperty("UserName", includeProperties)
			    && ! String.IsNullOrEmpty(objectToValidate.UserName))
			{
				if (this._userService.GetUserByUserName(objectToValidate.UserName) != null)
				{
					AddError("UserName", "UserNameValidatorNotUnique", true);
				}
			}
		}
	}
}