using System;
using System.Collections.Generic;
using Poc.Core.Entities.AdminModule;
using Poc.Core.Validation;
using Poc.DAL.Contracts;
using Poc.Service.AdminModule;

namespace Poc.Service.ModelValidators
{
	public class RoleModelValidator : CastleModelValidator<Role>
	{
		private readonly IUserService _userService;

		public RoleModelValidator(IUserService userService)
		{
		    _userService = userService;
		}

		protected override void PerformValidation(Role objectToValidate, ICollection<string> includeProperties)
		{
			base.PerformValidation(objectToValidate, includeProperties);

			// Check role name uniqueness.
			if (ShouldValidateProperty("Name", includeProperties)
				&& !String.IsNullOrEmpty(objectToValidate.Name))
			{
				var role = _userService.GetRoleByName(objectToValidate.Name);

				if (role != null && role.RoleId != objectToValidate.RoleId)
				{
					AddError("Name", "RoleNameValidatorNotUnique", true);
				}
			}
		}
	}
}