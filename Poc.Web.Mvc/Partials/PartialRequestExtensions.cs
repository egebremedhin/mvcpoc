﻿using System.Web.Mvc;

namespace Poc.Web.Mvc.Partials
{
	public static class PartialRequestsExtensions
	{
		public static void RenderPartialRequest(this HtmlHelper html, string viewDataKey)
		{
			var partial = html.ViewContext.ViewData.Eval(viewDataKey) as PartialRequest;
			if (partial != null)
			{
				partial.Invoke(html.ViewContext);
			}
		}
	}
}
