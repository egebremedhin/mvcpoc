﻿using System;
using System.Linq;
using System.Web.Routing;
using Poc.Web.Mvc.ViewModels;
using Poc.Core.Entities.AdminModule;
using Poc.Service.AdminModule;

namespace Poc.Web.Mvc.Controllers
{
	/// <summary>
	/// Base class for module controllers.
	/// </summary>
	public class ModuleController : SecureController
	{
		private Tab _currentTab;
        private Page _currentPage;
		private ITabService _tabService;
	    //private INodeService _nodeService;
		public ITabService TabService
		{
           set { this._tabService = value; }
		}

        //public INodeService NodeService { set { _nodeService = value; } }
		/// <summary>
		/// Gets the current node.
		/// </summary>
		protected Tab CurrentTab
		{
			get { return this._currentTab; }
		}

		/// <summary>
		/// Gets the current section.
		/// </summary>
		protected Page CurrentPage
		{
			get { return _currentPage; }
		}

		protected override void OnActionExecuting(System.Web.Mvc.ActionExecutingContext filterContext)
		{
			if (Request.Params["tabid"] != null)
			{
				_currentTab = _tabService.GetTabById(Int32.Parse(Request.Params["tabid"]));

                if (Request.Params["nodeid"] != null)
                {
                    int pageid = Int32.Parse(Request.Params["nodeid"]);
                    _currentPage = _currentTab.Pages.Single(x => x.PageId == pageid);
                }
			}

            base.OnActionExecuting(filterContext);
		}

		protected RouteValueDictionary GetNodeParams()
		{
			var nodeParams = new RouteValueDictionary();
		    if (_currentTab != null)
		    {
		        nodeParams.Add("tabid", _currentTab.TabId);
		    }
            if (this._currentPage != null)
			{
				nodeParams.Add("nodeid", this._currentPage.PageId);
			}
			
			return nodeParams;
		}

		protected ModuleAdminViewModel<T> GetModuleAdminViewModel<T>(T moduleData) where T: class
		{
			return new ModuleAdminViewModel<T>(CurrentTab,CurrentPage, PocContext, moduleData);
		}
	}
}
