﻿using System.Web.Mvc;
using Poc.Core.Entities.AdminModule;

namespace Poc.Web.Mvc.Controllers
{
	[Authorize(Order = 0)]
	public abstract class SecureController : BaseController
	{
		protected override void OnAuthorization(AuthorizationContext filterContext)
		{
		    var user = filterContext.HttpContext.User.Identity as AppUser;
		    if (user != null)
			{
				ViewData["PocUser"] = user;
			}
			base.OnAuthorization(filterContext);
		}
	}
}