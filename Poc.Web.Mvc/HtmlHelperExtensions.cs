﻿using System;
using System.Web.Mvc;
using Poc.Core;
using Poc.Core.Util;

namespace Poc.Web.Mvc
{
	/// <summary>
	/// Contains HtmlHelper extensions, specific for Poc.
	/// </summary>
	public static class HtmlHelperExtensions
	{
		/// <summary>
		/// Include a reference to javascript file. 
		/// </summary>
		/// <param name="htmlHelper"></param>
		/// <param name="scriptPath">The virtual path of the javascript file (supports ~/)</param>
		/// <returns>
		/// A <script> tag that references the given javascript file. 
		/// An empty string is returned when the reference has been returned earlier during the current request.
		/// </returns>
		/// <remarks>Tracks the path reference in HttpContext.Items to prevent duplicates.</remarks>
		public static string ScriptInclude(this HtmlHelper htmlHelper, string scriptPath)
		{
			if (IsAlreadyRegistered(htmlHelper, scriptPath))
			{
				return String.Empty;
			}
			string resolvedScriptPath = ResolveAndRegisterPath(htmlHelper, scriptPath);
			return string.Format("<script type=\"text/javascript\" src=\"{0}\"></script>", resolvedScriptPath);
		}

		/// <summary>
		/// Include a reference to a css file.
		/// </summary>
		/// <param name="htmlHelper"></param>
		/// <param name="cssPath">The virtual path of the css file (supports ~/)</param>
		/// <returns>
		/// A <link> tag that references the given css file.
		/// An empty string is returned when the reference has been returned earlier during the current request.
		/// </returns>
		/// <remarks>Tracks the path reference in HttpContext.Items to prevent duplicates.</remarks>
		public static string CssLink(this HtmlHelper htmlHelper, string cssPath)
		{
			if (IsAlreadyRegistered(htmlHelper, cssPath))
			{
				return String.Empty;
			}
			string resolvedCssPath = ResolveAndRegisterPath(htmlHelper, cssPath);
			return string.Format("<link rel=\"stylesheet\" href=\"{0}\" type=\"text/css\">", resolvedCssPath);
		}

		/// <summary>
		/// Include a reference to a css file.
		/// </summary>
		/// <param name="htmlHelper"></param>
		/// <param name="cssPath">The virtual path of the css file (supports ~/)</param>
		/// <returns>
		/// A <style> block that has an @import reference to the given css file.
		/// An empty string is returned when the reference has been returned earlier during the current request.
		/// </returns>
		/// <remarks>Tracks the path reference in HttpContext.Items to prevent duplicates.</remarks>
		public static string CssImport(this HtmlHelper htmlHelper, string cssPath)
		{
			if (IsAlreadyRegistered(htmlHelper, cssPath))
			{
				return String.Empty;
			}
			string resolvedCssPath = ResolveAndRegisterPath(htmlHelper, cssPath);
			return string.Format("<style type=\"text/css\">@import url({0});</style>", resolvedCssPath);
		}

		/// <summary>
		/// Gets the current Poc context.
		/// </summary>
		/// <param name="htmlHelper"></param>
		/// <returns></returns>
		public static IPocContext PocContext(this HtmlHelper htmlHelper)
		{
			return IoC.Resolve<IPocContext>();
		}

		private static bool IsAlreadyRegistered(HtmlHelper htmlHelper, string virtualPath)
		{
			return htmlHelper.ViewContext.HttpContext.Items.Contains(virtualPath);
		}

		private static string ResolveAndRegisterPath(HtmlHelper htmlHelper, string path)
		{
			var httpContext = htmlHelper.ViewContext.HttpContext;
			if (httpContext.Items.Contains(path))
			{
				throw new ArgumentException(string.Format("The path '{0}' is already registered.", path));
			}
			httpContext.Items.Add(path, String.Empty);
			var urlHelper = new UrlHelper(htmlHelper.ViewContext.RequestContext);
			return urlHelper.Content(path);
		}

        /// <summary>
        /// Creates a link that will open a jQuery UI dialog form.
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <param name="linkText">The inner text of the anchor element</param>
        /// <param name="dialogContentUrl">The url that will return the content to be loaded into the dialog window</param>
        /// <param name="dialogId">The id of the div element used for the dialog window</param>
        /// <param name="dialogTitle">The title to be displayed in the dialog window</param>
        /// <param name="updateTargetId">The id of the div that should be updated after the form submission</param>
        /// <param name="updateUrl">The url that will return the content to be loaded into the traget div</param>
        /// <returns></returns>
        public static MvcHtmlString DialogFormLink(this HtmlHelper htmlHelper, string linkText, string dialogContentUrl,
             string dialogTitle, string updateTargetId, string updateUrl)
        {
            var builder = new TagBuilder("a");
            builder.SetInnerText(linkText);
            builder.Attributes.Add("href", dialogContentUrl);
            builder.Attributes.Add("data-dialog-title", dialogTitle);
            builder.Attributes.Add("data-update-target-id", updateTargetId);
            builder.Attributes.Add("data-update-url", updateUrl);

            // Add a css class named dialogLink that will be
            // used to identify the anchor tag and to wire up
            // the jQuery functions
            builder.AddCssClass("dialogLink");

            return new MvcHtmlString(builder.ToString());
        }   

	}
}
