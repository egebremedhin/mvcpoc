﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;

using Poc.Core.Util;
using Poc.Web.Mvc.Localization;
using Poc.Web.Mvc.ViewModels;

namespace Poc.Web.Mvc.Filters
{
	public class MessagesFilter : ActionFilterAttribute
	{
		private MessageViewData _messageViewData;
		private TempDataDictionary _tempData;
		private ILocalizer _localizer;

		public MessagesFilter() : this(IoC.Resolve<ILocalizer>())
		{
		}

		public MessagesFilter(ILocalizer localizer)
		{
			_localizer = localizer;
		}

		public override void OnActionExecuting(ActionExecutingContext filterContext)
		{
			InitMessageViewData(filterContext);
			base.OnActionExecuting(filterContext);
		}

		public override void OnActionExecuted(ActionExecutedContext filterContext)
		{
			DisplayModelStateErrors(filterContext.Controller);
			base.OnActionExecuted(filterContext);
		}

		private void InitMessageViewData(ActionExecutingContext filterContext)
		{
			var currentController = filterContext.Controller;
			_tempData = currentController.TempData;
			if (_tempData.ContainsKey("Messages"))
			{
				_messageViewData = new MessageViewData((MessageViewData)_tempData["Messages"]);
			}
			else
			{
				_messageViewData = new MessageViewData();
			}
			_messageViewData.FlashMessageAdded += new EventHandler(MessageViewData_FlashMessageAdded);
			currentController.ViewData["Messages"] = _messageViewData;
		}

		private void MessageViewData_FlashMessageAdded(object sender, EventArgs e)
		{
			// HACK: Immediately store MessageViewData in TempData when a flash message is added. We can't do this
			// in OnActionExecuted because it is not fired in case of exceptions.
			_tempData["Messages"] = _messageViewData;
		}

		private void DisplayModelStateErrors(ControllerBase currentController)
		{
			if (!currentController.ViewData.ModelState.IsValid)
			{
				string generalMessage = _localizer.GetString("ModelValidationErrorMessage");
				var errorList = new TagBuilder("ul");
                
				var errorSummary = new StringBuilder();
				foreach (KeyValuePair<string, ModelState> modelStateKvp in currentController.ViewData.ModelState)
				{
					foreach (ModelError modelError in modelStateKvp.Value.Errors)
					{
						var listItem = new TagBuilder("li");
						string baseName = String.Format("{0}.globalresources"
							, currentController.GetType().Namespace.Replace(".Controllers", String.Empty).ToLowerInvariant());
						listItem.SetInnerText(_localizer.GetString(modelError.ErrorMessage, baseName));
						errorSummary.AppendLine(listItem.ToString(TagRenderMode.Normal));
					}
				}
				errorList.InnerHtml = errorSummary.ToString();
				_messageViewData.AddErrorMessage(generalMessage + errorList.ToString(TagRenderMode.Normal));
			}
			_messageViewData.FlashMessageAdded -= new EventHandler(MessageViewData_FlashMessageAdded);
		}
	}
}
