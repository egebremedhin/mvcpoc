﻿using System;
using System.Security;
using System.Web.Mvc;
using Poc.Core.Entities.AdminModule;

namespace Poc.Web.Mvc.Filters
{
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
	public class PermissionFilterAttribute : FilterAttribute, IAuthorizationFilter
	{
		private string _rights;
		private string[] _rightsArray;

		/// <summary>
		/// The required rights as a comma-separated string.
		/// </summary>
		public string RequiredRights
		{
			get { return _rights; }
			set { SetRights(value); }
		}

		/// <summary>
		/// Array of required rights.
		/// </summary>
		public string[] RightsArray
		{
			get { return this._rightsArray; }
		}

		private void SetRights(string rightsAsString)
		{
			this._rights = rightsAsString;
			this._rightsArray = rightsAsString.Split(new char[1] { ',' });
		}

		public void OnAuthorization(AuthorizationContext filterContext)
		{
			// Only check authorization when rights are defined and the user is authenticated.
		    if (filterContext.HttpContext.User.Identity.IsAuthenticated)
		    {
		        var pocUser = filterContext.HttpContext.User.Identity  as AppUser;

		        if (pocUser == null)
		        {
		            throw new SecurityException("UserNullException");
		        }

                //if (!pocUser.)
                //{
                //    throw new SecurityException("ActionNotAllowedException");
                //}
            }
		}
	}
}