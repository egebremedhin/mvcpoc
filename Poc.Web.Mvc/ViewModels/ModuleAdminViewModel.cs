﻿using System.Web.Routing;
using Poc.Core;
using Poc.Core.Entities.AdminModule;

namespace Poc.Web.Mvc.ViewModels
{
	public class ModuleAdminViewModel<T> where T: class
	{
        public Tab PocTab { get;private set; }
		public Page PocPage { get; private set; }
		    
		public IPocContext PocContext { get; private set; }
		public T ModuleData { get; private set; }

		public ModuleAdminViewModel(Tab tab,Page page ,IPocContext pocContext, T moduleData)
		{
			PocTab = tab;
			PocPage = page;
			PocContext = pocContext;
			ModuleData = moduleData;
		}

		public RouteValueDictionary GetNodeParams()
		{
			var nodeParams = new RouteValueDictionary();
            if (PocTab != null)
            {
                nodeParams.Add("tabid", PocTab.TabId);
            }
			if (PocPage != null)
			{
                nodeParams.Add("nodeid", PocPage.PageId);
			}
			return nodeParams;
		}
	}

	/// <summary>
	/// 
	/// </summary>
	public static class RouteValueDictionaryExtensions
	{
		/// <summary>
		/// Merge a given key value pair with the RouteValueDictionary. 
		/// </summary>
		/// <param name="routeValueDictionary"></param>
		/// <param name="key"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		public static RouteValueDictionary Merge(this RouteValueDictionary routeValueDictionary, string key, object value)
		{
			if (! routeValueDictionary.ContainsKey(key))
			{
				routeValueDictionary.Add(key, value);
			}
			else
			{
				routeValueDictionary[key] = value;
			}
			return routeValueDictionary;
		}
	}
}
