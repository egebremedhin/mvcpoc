﻿using System.Web.Routing;

namespace Poc.Web.Mvc
{
    public interface IMvcModule
    {
    	void RegisterRoutes(RouteCollection routes);
    }
}
