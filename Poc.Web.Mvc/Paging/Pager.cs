﻿using System;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;

namespace Poc.Web.Mvc.Paging
{
	public class Pager
	{
		private ViewContext viewContext;
		private readonly int _pageSize;
		private readonly int _currentPage;
		private readonly int _totalItemCount;
		private readonly RouteValueDictionary _linkWithoutPageValuesDictionary;

		public Pager(ViewContext viewContext, int pageSize, int currentPage, int totalItemCount, RouteValueDictionary valuesDictionary)
		{
			this.viewContext = viewContext;
			this._pageSize = pageSize;
			this._currentPage = currentPage;
			this._totalItemCount = totalItemCount;
			this._linkWithoutPageValuesDictionary = valuesDictionary;
		}

		public string RenderHtml()
		{
			int pageCount = (int)Math.Ceiling(this._totalItemCount / (double)this._pageSize);
			int nrOfPagesToDisplay = 10;

			var sb = new StringBuilder();
			
			// Previous
			if (this._currentPage > 1)
			{
				sb.Append(GeneratePageLink("&lt;", this._currentPage - 1));
			}
			else
			{
				sb.Append("<span class=\"disabled\">&lt;</span>");
			}

			int start = 1;
			int end = pageCount;

			if (pageCount > nrOfPagesToDisplay)
			{
				int middle = (int)Math.Ceiling(nrOfPagesToDisplay / 2d) - 1;
				int below = (this._currentPage - middle);
				int above = (this._currentPage + middle);

				if (below < 4)
				{
					above = nrOfPagesToDisplay;
					below = 1;
				}
				else if (above > (pageCount - 4))
				{
					above = pageCount;
					below = (pageCount - nrOfPagesToDisplay);
				}

				start = below;
				end = above;
			}

			if (start > 3)
			{
				sb.Append(GeneratePageLink("1", 1));
				sb.Append(GeneratePageLink("2", 2));
				sb.Append("...");
			}
			for (int i = start; i <= end; i++)
			{
				if (i == this._currentPage)
				{
					sb.AppendFormat("<span class=\"current\">{0}</span>", i);
				}
				else
				{
					sb.Append(GeneratePageLink(i.ToString(), i));
				}
			}
			if (end < (pageCount - 3))
			{
				sb.Append("...");
				sb.Append(GeneratePageLink((pageCount - 1).ToString(), pageCount - 1));
				sb.Append(GeneratePageLink(pageCount.ToString(), pageCount));
			}

			// Next
			if (this._currentPage < pageCount)
			{
				sb.Append(GeneratePageLink("&gt;", (this._currentPage + 1)));
			}
			else
			{
				sb.Append("<span class=\"disabled\">&gt;</span>");
			}
			return sb.ToString();
		}

		private string GeneratePageLink(string linkText, int pageNumber)
		{
			var pageLinkValueDictionary = new RouteValueDictionary(this._linkWithoutPageValuesDictionary);
			pageLinkValueDictionary.Add("page", pageNumber);
			var virtualPathData = RouteTable.Routes.GetVirtualPath(this.viewContext.RequestContext, pageLinkValueDictionary);

			if (virtualPathData != null)
			{
				string linkFormat = "<a href=\"{0}\">{1}</a>";
				return String.Format(linkFormat, virtualPathData.VirtualPath, linkText);
			}
			else
			{
				return null;
			}
		}
	}
}